// Copyright (c) 2018 UNBC
// Copyright (c) 2015 Pareto Software, LLC
// Released under an MIT license: http://opensource.org/licenses/MIT
// Based on original example form at https://formden.com/blog/conditional-form-field
$( document ).ready(function() {
		// Wrapper for description output field
		var description = $( "div.rad-description" );
		description.text( "RAD Physical Description" ); // set initial RAD Description text
		
		// Ok, so this next little section is 'fun'
		// The purpose is to dynamically create a bunch of wrappers for all the input fields
		// It (ab)uses the eval function to generate two variables for each uniquely named input area
		// One variable for the inputs themselves, and one for the parent div
		// The primary variable is used in all the many functions below, for monitoring activity and getting values
		// The _parent variable is used for hiding and showing the entire input div
		var all_inputs = $('#live_form input'); // get all the input elements in the form into a single array(?)
		var unique_names = [] // stores unique input names, so that we aren't trying to create duplicate variables
		// This loops through every single input element on the page
		all_inputs.each(function ( index ) {			
			current_name = $(this).context.name // gets name of current object
			current_input = $(this).context.type // gets input type of current object

			// Check if we've hit a new input div (new name), and make new variables for that form input, and its parent
			if (!(unique_names.includes(current_name))){
				unique_names.push(current_name); // add current unique name to the unique_names array
				
				// Create a jQuery object variable for the inputs matching the current unique name
				eval(current_name + "=$('#live_form input:" + current_input + "[name=" + current_name + "]');");

				// Create a jQuery object variable for the parent object (for hiding and displaying the entire input div
				// Text inputs are not nested as deep as radio/checkbox/etc
				if (current_input == "text"){
					eval(current_name + "_parent=$('#live_form input:" + current_input + "[name=" + current_name + "]').parent();");
				}
				else{
					eval(current_name + "_parent=$('#live_form input:" + current_input + "[name=" + current_name + "]').parent().parent().parent().parent();");
				}
			}	
		});
		
		// Variable containing all _parent object variables, for hiding everything
		var all=gm_parent // get the variable started
		// loop through all variables in the window
		for (var key in window) {
			// check if the variable name contains '_parent'
			if (key.indexOf("_parent") >= 0){
				// add the variable to the 'all' variable. eval is used to add the actual var, not just the text name
				all = all.add(eval(key));
			}
		}
		all = all.not(desc_type_parent); // we don't need this in the all variable
		all.addClass('hidden'); //hide all the form questions. They will be revealed as needed
		
		// Variable containing _parent objects that should be shown
		var show=desc_type_parent;
		
		// Prevents non-decimal input into text field
		// Requires the text input to have the class 'numeric_with_decimal'
		$(".numeric_with_decimal").on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
			if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
				event.preventDefault();
			}
        });
		
		// Prevents non-integer input into text field
		// Requires the text input to have the class 'numeric_no_decimal'
		$(".numeric_no_decimal").on("keypress keyup blur",function (event) {    
			$(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
		
		//
		// The functions below are for setting the RAD description
		//
		
		// Description function for Textual Records
		function tr_set_description(){
			var description_text = "Placeholder: Textual Records";
			var linear_ext = tr.val(); // linear extent (length) of records
			var linear_ext_msmt = tr_le.filter(":checked").val(); // centimetres or metres
			var granular_type = tr_le_m.filter(":checked").val(); // Type of granular measurement
			var granular_amt = tr_le_m_gm.val(); // Amount of granular measurement
			var size_amt = tr_le_m_gm_amt.filter(":checked").val(); // # of size dimensions
			var width1 = tr_le_m_gm_amt_width1.val(); // width 1
			var height1 = tr_le_m_gm_amt_height1.val(); // height 1
			var width2 = tr_le_m_gm_amt_width2.val(); // width 2
			var height2 = tr_le_m_gm_amt_height2.val(); // height 2
			
			if (linear_ext && linear_ext_msmt){
				description_text = linear_ext + " " + linear_ext_msmt + " of textual records";
				
				if (granular_type && granular_amt){
					description_text += " (" + granular_amt + " " + granular_type.toLowerCase();
					
					if (size_amt && width1 && height1 && !(size_amt == "2" && (!width2 || !height2))){
						description_text += " ; " + height1 + " x " + width1 + " cm";
						if (size_amt == "3 or more"){
							description_text += " or smaller";
						}
						else if (size_amt == "2" && width2 && height2){
							description_text += " and " + height2 + " x " + width2 + " cm";
						}
					}
					description_text += ")";
				}	
			}
			
			description.text(description_text); // Write the description text to the page
		};
		
		// Description function for Graphic Materials > Collage
		function gm_c_set_description(){
			var description_text = "";
			var num_coll = gm_c.val(); // number of collages
			var num_coll_aprx = gm_c_num_aprx.filter( ":checked" ).val(); // is collage num approx?
			//var collage_medium = gm_c_num.filter( ":checked" ).val(); // which mediums?
			var coll_med = gm_c_num.filter( ":checked" ); // which mediums?
			var coll_med_unknown_check = gm_c_num.filter('[value=Unknown]').prop("checked"); // is 'Unknown' checked?
			var coll_size_num = gm_c_num_med.filter( ":checked" ).val(); // collage size number
			var width1 = gm_c_num_med_width1.val();
			var height1 = gm_c_num_med_height1.val();
			var width2 = gm_c_num_med_width2.val();
			var height2 = gm_c_num_med_height2.val();
			var coll_mnt = gm_c_num_cha_med_size.filter( ":checked" ).val(); // collage mount type
			var mnt_width = gm_c_num_cha_med_size_width.val();
			var mnt_height = gm_c_num_cha_med_size_height.val();
			
			if ( num_coll_aprx == "Yes" ){
				description_text += "ca. ";
			}
			description_text += num_coll + " collage";
			if ( num_coll > 1 ){ description_text += "s"; }
			
			// Is a Medium selected?
			if (coll_med.length > 0){
				// Make sure 'Unknown' medium isn't checked
				if ( !(coll_med_unknown_check) ){
					description_text += " : ";
					
					// Function to add the 'Medium' values programmatically
					coll_med.each( function(i) {
						// first medium
						if ( i == 0 ){
							description_text += this.value.toLowerCase();
						}
						// second medium
						else if ( i == 1 ){
							if ( coll_med.length == 2 ){
								description_text += " and ";
							}
							else{
								description_text += ", ";
							}
							description_text += this.value.toLowerCase();
						}
						// third medium
						else if ( i == 2 ){
							description_text += ", and " + this.value.toLowerCase();
						}
					});
				}
				
				// Are size dimensions provided?
				if ( coll_size_num ){
					if ( width1 && height1 ){
						if ( coll_size_num == "1"){
							description_text += " ; "+height1+" x "+width1+" cm";  
						}
						else if ( coll_size_num == "2" && width2 && height2 ){
							description_text+=" ; "+height1+" x "+width1+" cm and "+height2+" x "+width2+" cm";
						}
						else if ( coll_size_num == "3 or more" ){
							description_text += " ; "+height1+" x "+width1+" cm or smaller";  
						}
					}
					
					// Is mount type provided?
					if ( coll_mnt ){
						if ( coll_mnt.match("Sheet|Card|Mat") ){
							description_text += " on "
						}
						else if ( coll_mnt.match("Frame|Case") ){
							description_text += " in "
						}
						description_text+=coll_mnt.toLowerCase();
						
						// Are mount dimentions provided?
						if ( mnt_width && mnt_height ){
							description_text += " "+mnt_height+" x "+mnt_width+" cm";  
						}
					}	
				}
			}
			description.text(description_text); // Write the description text to the page
		}
		
		// Description function for Graphic Materials > Drawing
		function gm_d_set_description(){
			var description_text = "";
			
			var num_dr = gm_d.val(); // number of drawings
			var num_dr_aprx = gm_d_num_aprx.filter( ":checked" ).val(); // is drawing num approx?
			var dr_type = gm_d_num.filter( ":checked" ).val(); // drawing type
			var dr_type_var_check = gm_d_num.filter('[value=Various]').prop("checked"); // is 'Various' checked?
			var dr_type_unk_check = gm_d_num.filter('[value=Unknown]').prop("checked"); // is 'Unknown' checked?
			var dr_med = gm_d_num_type.filter( ":checked" ).val(); // drawing medium
			var dr_med_unk_check = gm_d_num_type.filter('[value=Unknown]').prop("checked"); // is 'Unknown' checked?
			var dr_size_num = gm_d_num_type_med.filter( ":checked" ).val(); // how many size dimensions?
			var width1 = gm_d_num_type_med_width1.val();
			var height1 = gm_d_num_type_med_height1.val();
			var width2 = gm_d_num_type_med_width2.val();
			var height2 = gm_d_num_type_med_height2.val();
			var dr_mnt = gm_d_num_type_med_size.filter( ":checked" ).val(); // what kind of mount?
			var mnt_width = gm_d_num_type_med_size_width.val();
			var mnt_height = gm_d_num_type_med_size_height.val();
			
			if ( num_dr_aprx == "Yes" ){
				description_text += "ca. ";
			}
			description_text += num_dr + " drawing";
			if ( num_dr > 1 ){ description_text+="s"; }
			
			// Is a Type selected?
			if ( dr_type ){
				// Make sure Type isn't 'Various' or 'Unknown'
				if ( !dr_type_var_check && !dr_type_unk_check ){
					description_text += " : " + dr_type.toLowerCase();
				}
				
				// Is a Medium selected?
				if ( dr_med ){
					// Make sure 'Unknown' isn't selected
					if ( !dr_med_unk_check ){
						if ( dr_type_var_check || dr_type_unk_check ){
							description_text += " : " + dr_med.toLowerCase();
						}
						else{
							description_text += ", " + dr_med.toLowerCase();
						}
					}
					
					// Is # of Dimensions provided?
					if ( dr_size_num ){
						if ( width1 && height1){
							if ( dr_size_num == "1" ){
								description_text += " ; "+height1+" x "+width1+" cm"; 
							}
							else if ( dr_size_num == "2" && width2 && height2 ){
								description_text += " ; "+height1+" x "+width1+" cm and "+height2+" x "+width2+" cm";
							}
							else if ( dr_size_num == "3 or more" ){
								description_text += " ; "+height1+" x "+width1+" cm or smaller";  
							}
						}
						
						// Is mount type selected?
						if ( dr_mnt ){
							if ( dr_mnt.match("Sheet|Card|Mat") ){
							description_text += " on "
							}
							else if ( dr_mnt.match("Frame|Case") ){
								description_text += " in "
							}
							description_text+=dr_mnt.toLowerCase();
							
							// Are mount dimentions provided?
							if ( mnt_width && mnt_height ){
								description_text += " "+mnt_height+" x "+mnt_width+" cm";  
							}
						}
					}
				}
			}
			
			description.text(description_text); // Write the description text to the page
		}
		
		
		// Description function for Graphic Materials > Painting
		function gm_p_set_description(){
			var description_text = "";
			
			var num_p = gm_p.val(); // number of paintings
			var num_p_aprx = gm_p_num_aprx.filter( ":checked" ).val(); // is painting num approx?
			var p_type = gm_p_num.filter( ":checked" ).val(); // painting type
			var p_type_unk_check = gm_p_num.filter('[value=Unknown]').prop("checked"); // is 'Unknown' checked?
			var p_med = gm_p_num_type.filter( ":checked" ).val(); // Medium
			var p_med_unk_check = gm_p_num_type.filter('[value=Unknown]').prop("checked"); // is 'Unknown' checked?
			var p_base = gm_p_num_type_med.filter( ":checked" ).val(); // Base
			var p_base_unk_check = gm_p_num_type_med.filter('[value=Unknown]').prop("checked"); // is 'Unknown' checked?
			var p_size_num = gm_p_num_type_med_base.filter( ":checked" ).val(); // how many size dimensions?
			var width1 = gm_p_num_type_med_base_width1.val();
			var height1 = gm_p_num_type_med_base_height1.val();
			var width2 = gm_p_num_type_med_base_width2.val();
			var height2 = gm_p_num_type_med_base_height2.val();
			var p_mnt = gm_p_num_type_med_base_size.filter( ":checked" ).val(); // what kind of mount?
			var mnt_width = gm_p_num_type_med_base_size_width.val();
			var mnt_height = gm_p_num_type_med_base_size_height.val();
			
			if ( num_p_aprx == "Yes" ){
				description_text += "ca. ";
			}
			description_text += num_p + " painting";
			if ( num_p > 1 ){ description_text+="s"; }
			
			// Is a Type selected?
			if ( p_type ){
				// Make sure Type isn't 'Unknown'
				if ( !p_type_unk_check ){
					description_text += " : " + p_type.toLowerCase();
				}
				
				// Is a Medium selected?
				if ( p_med ){
					// Make sure 'Unknown' isn't selected
					if ( !p_med_unk_check ){
						if ( p_type_unk_check ){
							description_text += " : " + p_med.toLowerCase();
						}
						else{
							description_text += ", " + p_med.toLowerCase();
						}
					}
					
					// Is a Base selected, or is Medium Unknown?
					if ( p_base || p_med_unk_check ){
						// Make sure Base isn't 'Unknown' and Medium isn't 'Unknown'
						if ( !p_base_unk_check && !p_med_unk_check ){
							if ( p_type_unk_check && p_med_unk_check ){
								description_text += " : ";
							}
							description_text += " on "+p_base.toLowerCase();
						}
					
						// Is # of Dimensions provided?
						if ( p_size_num ){
							if ( width1 && height1){
								if ( p_size_num == "1" ){
									description_text += " ; "+height1+" x "+width1+" cm"; 
								}
								else if ( p_size_num == "2" && width2 && height2 ){
									description_text += " ; "+height1+" x "+width1+" cm and "+height2+" x "+width2+" cm";
								}
								else if ( p_size_num == "3 or more" ){
									description_text += " ; "+height1+" x "+width1+" cm or smaller";  
								}
							}
							
							// Is mount type selected?
							if ( p_mnt ){
								if ( p_mnt.match("Sheet|Card|Mat") ){
								description_text += " on "
								}
								else if ( p_mnt.match("Frame|Case") ){
									description_text += " in "
								}
								description_text+=p_mnt.toLowerCase();
								
								// Are mount dimentions provided?
								if ( mnt_width && mnt_height ){
									description_text += " "+mnt_height+" x "+mnt_width+" cm";  
								}
							}
						}
					}
				}
			}
			
			description.text(description_text); // Write the description text to the page
		}
		
		
		// Description function for Graphic Materials > Photograph > Photograph
		function gm_ph_ph_set_description(){
			var description_text = "";
			var num_photos = parseInt(gm_ph_ph.val(), 10);
			var num_photos_apprx = gm_ph_ph_num_aprx.filter( ":checked" ).val(); // Is the photo # approximate?
			var num_photo_types = gm_ph_ph_num.filter(":checked").length; // # checked in 'what types of photographs?'
			
			// Print variables //
			var print_check = gm_ph_ph_num.filter('[value=Print]').prop("checked"); //is 'Print' checked?
			var num_prints = parseInt(gm_ph_ph_num_p_num.val() , 10); // Number of Prints
			var print_cha = gm_ph_ph_num_p.filter(":checked").val(); // which Print 'Colour Characteristic'?
			var print_type = gm_ph_ph_num_p_cha.filter(":checked").val(); // which Print 'Type'?
			var print_size_amt = gm_ph_ph_num_p_cha_typ.filter(":checked").val(); // how many print sizes?
			var print_width1 = gm_ph_ph_num_p_cha_typ_width1.val();
			var print_height1 = gm_ph_ph_num_p_cha_typ_height1.val();
			var print_width2 = gm_ph_ph_num_p_cha_typ_width2.val();
			var print_height2 = gm_ph_ph_num_p_cha_typ_height2.val();
			var print_mnt = gm_ph_ph_num_p_cha_typ_size.filter(":checked").val(); // Print mount?
			var print_mnt_width = gm_ph_ph_num_p_cha_typ_size_width.val();
			var print_mnt_height = gm_ph_ph_num_p_cha_typ_size_height.val();
			
			// Negative variables //
			var neg_check = gm_ph_ph_num.filter('[value=Negative]').prop("checked"); //is 'Negative' checked?
			var num_negs = parseInt(gm_ph_ph_num_n_num.val(), 10); // Number of Negatives
			var neg_film_check = gm_ph_ph_num_n.filter('[value=Film]').prop("checked"); // is 'Film' checked?
			var neg_glass_check = gm_ph_ph_num_n.filter('[value=Glass]').prop("checked"); // is 'Glass' checked?
			var neg_film_cha = gm_ph_ph_num_n_f.filter(":checked").val(); // which negative film 'Colour Characteristic'?
			var neg_film_nit = gm_ph_ph_num_n_f_b.filter(":checked").val(); // is the negative film nitrate?
			var neg_film_format_check_35 = gm_ph_ph_num_n_f_cha.filter('[value=35mm]').prop("checked"); // is '35mm' checked?
			//var neg_film_format_check_other = gm_ph_ph_num_n_f_cha.filter('[value=Other]').prop("checked"); // is 'Other' checked?
			var neg_glass_type = gm_ph_ph_num_n_g.filter(":checked").val(); // which negative glass plate type?
			var neg_size_amt = gm_ph_ph_num_n_med.filter(":checked").val(); // how many negative size dimensions?
			var neg_width1 = gm_ph_ph_num_n_med_width1.val();
			var neg_height1 = gm_ph_ph_num_n_med_height1.val();
			var neg_width2 = gm_ph_ph_num_n_med_width2.val();
			var neg_height2 = gm_ph_ph_num_n_med_height2.val();
			
			// Slide variables //
			var slide_check = gm_ph_ph_num.filter('[value=Slide]').prop("checked"); //is 'Slide' checked?
			var num_slides = parseInt(gm_ph_ph_num_s.val(), 10);
			var slide_cha_check_num = gm_ph_ph_num_s_num.filter(":checked").length; // # checked in 'slide colour characteristics'
			var slide_cha_bw = gm_ph_ph_num_s_num.filter('[value="b&w"]').prop("checked"); //is 'b&w' checked?
			var slide_cha_col = gm_ph_ph_num_s_num.filter('[value="col."]').prop("checked"); //is 'col.' checked?
			var slide_format_check_num = gm_ph_ph_num_s_num_cha.filter(":checked").length; // # checked in 'slide format'
			var slide_format_check_35 = gm_ph_ph_num_s_num_cha.filter('[value="35 mm"]').prop("checked");
			var slide_format_check_110 = gm_ph_ph_num_s_num_cha.filter('[value="110 Format (Image 13 x 17 mm)"]').prop("checked");
			var slide_format_check_35half = gm_ph_ph_num_s_num_cha.filter('[value="35 mm Half Frame (Image 18 mm x 24 mm)"]').prop("checked");
			var slide_format_check_126 = gm_ph_ph_num_s_num_cha.filter('[value="126 Format (Image 28 x 28 mm)"]').prop("checked");
			var slide_format_check_127 = gm_ph_ph_num_s_num_cha.filter('[value="127 Format (Image 40 x 40 mm)"]').prop("checked");
			var slide_format_check_super = gm_ph_ph_num_s_num_cha.filter('[value="Superslide (Image 46 x 46 mm)"]').prop("checked");
			var slide_format_check_120 = gm_ph_ph_num_s_num_cha.filter('[value="120 Format / Medium Format (Image smaller than 10 x 12 cm)"]').prop("checked");
			var slide_format_check_large = gm_ph_ph_num_s_num_cha.filter('[value="Large Format (Image 10 x 12 cm or larger)"]').prop("checked");
			var slide_format_1 = ""
			var slide_format_2 = ""	
			
			// Start building the Description text
			if (num_photos_apprx == "Yes"){description_text+="ca. ";} // add '.ca' to description as needed
			description_text += num_photos + " photograph"; // add the number of photographs to description text
			if (num_photos > 1){
				description_text += "s" ;// add an 's' if multiple photos
			}
			
			if ( num_photo_types > 0 ){
				
				if ( num_photo_types > 1 ){
					description_text += " ("
				}
							
				// Prints //
				
				//Make sure Number of Prints is provided, if more than 1 photo type selected
				if ( print_check && ( num_photo_types == 1 || num_prints ) ){
					// Multiple photo types?
					if ( num_photo_types > 1 ){
						description_text += num_prints + " prints ";
					}
					
					// Is a Colour Characteristic selected?
					if ( print_cha ){
						description_text += " : ";
						if ( print_cha == "Black & White" ){
							description_text+="b&w";
						}
						else if ( print_cha == "Colour" ){
							description_text+="col.";
						}
						else if ( print_cha == "Both" ){
							description_text+="b&w and col.";
						}
						
						// Is a Print Type selected?
						if ( print_type ){
							if ( print_type == "Daguerreotype (1839-1860)"){
								description_text+=" daguerreotype";
							}
							else if ( print_type == "Cyanotype (1842-1950)"){
								description_text+=" cyanotype";
							}
							else if ( print_type == "Ambrotype (1854-1865)"){
								description_text+=" ambrotype";
							}
							else if ( print_type == "Tintype (1856-1920)"){
								description_text+=" tintype";
							}
							else if ( print_type == "Albumen (1860-1895)"){
								description_text+=" albumen print";
							}
							else if ( print_type == "Carbon (1860-1950)"){
								description_text+=" carbon print";
							}
							else if ( print_type == "Platinum (1880-1930)"){
								description_text+=" platinum print";
							}
							else if ( print_type == "Collodion Printing-Out Paper (1885-1910)"){
								description_text+=" collodion printing-out paper";
							}
							else if ( print_type == "Gelatin Printing-Out Paper (1885-1910)"){
								description_text+=" gelatin printing-out paper";
							}
							else if ( print_type == "Gum Dichromate (1890-1930)"){
								description_text+=" gum dichromate print";
							}
							else if ( print_type == "Silver Gelatin (1890-2000)"){
								description_text+=" silver gelatin print";
							}
							else if ( print_type == "Matte Collodion (1893-1910)"){
								description_text+=" matte collodion print";
							}
							else if ( print_type == "Bromoil (1907-1930)"){
								description_text+=" bromoil print";
							}
							else if ( print_type == "Carbro (1920-1950)"){
								description_text+=" carbo print";
							}
							else if ( print_type == "Dye Transfer (1946-1990)"){
								description_text+=" dye transfer print";
							}
							else if ( print_type == "Chromogenic (1941-)"){
								description_text+=" chromogenic print";
							}
							else if ( print_type == "Cibachrome (1963-2012)"){
								description_text+=" cibachrome print";
							}
							else if ( print_type == "Instant Diffusion Transfer (1948-2008)"){
								description_text+=" instant diffusion transfer print";
							}
							else if ( print_type == "Instant Dye Diffusion Transfer (1963-2008)"){
								description_text+=" instant dye diffusion transfer print";
							}
							else if ( print_type == "Instant Internal Dye Diffusion Transfer (1972-2008)"){
								description_text+=" instant internal dye diffusion transfer print";
							}
							
							if ( ( num_prints > 1 || ( isNaN(num_prints) && num_photos > 1 ) ) && !(print_type.match("Various|Unknown") ) ){
								description_text+="s";
							}
							
							// Is print size amount specified?
							if ( print_size_amt ){
								// Are print dimentions provided
								if ( print_width1 && print_height1 ){
									if ( print_size_amt == "1" ){
										description_text+=" ; "+print_height1+" x "+print_width1+" cm";
									}
									else if ( print_size_amt == "2" && print_width2 && print_height2){
										description_text+=" ; "+print_height1+" x "+print_width1+" cm and "+print_height2+" x "+print_width2+" cm";
									}
									else if ( print_size_amt == "3 or more" ){
										description_text+=" ; "+print_height1+" x "+print_width1+" cm or smaller";
									}
								}
								
								// Is it mounted?
								if ( print_mnt ){
									if ( print_mnt.match("Sheet|Card") ){
										description_text+=" on ";
									}
									else if ( print_mnt.match("Frame|Case") ){
										description_text+=" in ";
									}
									description_text+=print_mnt.toLowerCase();
									
									// Are mount dimensions provided?
									if ( print_mnt_width && print_mnt_height ){
										description_text+=" "+print_mnt_height+" x "+print_mnt_width+" cm"
									}
								}
							}
						}
					}
				}
				
				// Negatives //
				
				//Make sure Number of Negatives is provided, if more than 1 photo type selected
				if ( neg_check && ( num_photo_types == 1 || num_negs ) ){
					// Multiple photo types?
					if ( num_photo_types > 1 ){
						if ( print_check ){
							description_text += " - ";
						}
						description_text += num_negs + " negatives ";
					}
					
					// What medium (film or glass)?
					if ( neg_film_check || neg_glass_check ){
						description_text += " : ";
						
						// 'Film' is checked as a negative medium
						if ( neg_film_check && neg_film_cha ){
							// film includes nitrate
							if ( neg_film_nit == "Yes" ){
								if ( neg_film_cha == "Black & White" ){
									description_text += "b&w nitrate negative"
									if ( num_photos > 1){ description_text += "s"; }
								}
								else if ( neg_film_cha == "Both" ){
									description_text += "b&w and col. negatives, some nitrate";
								}
							}
							// film does not include nitrate
							else{
								if ( neg_film_cha == "Black & White" ){
									description_text += "b&w negative";
								}
								else if ( neg_film_cha == "Colour" ){
									description_text += "col. negative";
								}
								else if ( neg_film_cha == "Both" ){
									description_text += "b&w and col. negative";
								}
								
								if (num_photos > 1){ description_text += "s"; }
							}
						}
						
						if ( neg_film_check && neg_film_cha && neg_glass_check && neg_glass_type ){
							description_text += " and ";
						}
						
						// 'Glass' is checked as a negative medium'
						if ( neg_glass_check && neg_glass_type ){
							if ( neg_glass_type == "Collodion Glass Plate (ca. 1851-1885)" ){
								description_text += "b&w collodion glass plate negative";
							}
							else if ( neg_glass_type == "Gelatin Dry Plate (ca. 1878-1925)" ){
								description_text += "b&w gelatin dry glass plate negative";
							}
							
							if (neg_glass_type && num_photos > 1){ description_text += "s"; }
						}
						
						// Add sizes, if provided
						if ( neg_film_format_check_35 ){
							description_text += " ; 35 mm";
							if ( neg_size_amt && neg_width1 && neg_height1 ){
								description_text += " and "+neg_height1+" x "+neg_width1+" cm";
								if ( neg_size_amt != "1" ){
									description_text += " or smaller";
								}
							}
						}
						else if ( neg_size_amt && neg_width1 && neg_height1 ){
							if ( neg_size_amt != "2" ){
								description_text += " ; "+neg_height1+" x "+neg_width1+" cm"
								if ( neg_size_amt == "3 or more" ){
									description_text += " or smaller";
								}
							}
							else if ( neg_size_amt == "2" && neg_width2 && neg_height2 ){
								description_text+=" ; "+neg_height1+" x "+neg_width1 + " cm and "+neg_height2+" x "+neg_width2 + " cm";
							}
						}
						
					}
					
				}
				
				// Slides //
				//Make sure Number of Slides is provided, if more than 1 photo type selected
				if ( slide_check && ( num_photo_types == 1 || num_slides ) ) {
					// Multiple photo types?
					if ( num_photo_types > 1 ){
						if ( print_check || neg_check ){
							description_text += " - ";
						}
						description_text += num_slides + " slides ";
					}
					
					// What slide colour characteristics?
					if ( slide_cha_check_num == 2 ){ 
						description_text+=" : b&w and col.";
					}
					else if ( slide_cha_check_num == 1 ){
						if ( slide_cha_bw ){ description_text+=" : b&w"; }
						else if ( slide_cha_col ){ description_text+=" : col."; }
						else { description_text = "Logic Error - Slide Colour Characteristics section"; }
					}
					
					// Add the word slide(s) to the description
					if ( slide_cha_check_num >= 1 ){
						description_text += " slide";
						if (num_photos > 1){
							description_text += "s" ;// add an 's' if multiple photos
						}	
					}
					
					// What slide format(s)?
					// Surely there's a cleaner/easier way to do this?
					if ( slide_format_check_num >= 1 ){
						description_text += " ; ";
						if (slide_format_check_35){
							slide_format_1 = "35 mm";
						}
						if (slide_format_check_110){
							if (slide_format_1 != ""){slide_format_2 = "1.3 x 1.7 cm" }
							else{slide_format_1 = "1.3 x 1.7 cm" }
						}
						if (slide_format_check_35half){
							if (slide_format_1 != ""){slide_format_2 = "35 mm half frame" }
							else{slide_format_1 = "35 mm half frame" }
						}
						if (slide_format_check_126){
							if (slide_format_1 != ""){slide_format_2 = "2.8 x 2.8 cm" }
							else{slide_format_1 = "2.8 x 2.8 cm" }
						}
						if (slide_format_check_127){
							if (slide_format_1 != ""){slide_format_2 = "4 x 4 cm" }
							else{slide_format_1 = "4 x 4 cm" }
						}
						if (slide_format_check_super){
							if (slide_format_1 != ""){slide_format_2 = "4.6 x 4.6 cm" }
							else{slide_format_1 = "4.6 x 4.6 cm" }
						}
						if (slide_format_check_120){
							if (slide_format_1 != ""){slide_format_2 = "medium format" }
							else{slide_format_1 = "medium format" }
						}
						if (slide_format_check_large){
							if (slide_format_1 != ""){slide_format_2 = "large format" }
							else{slide_format_1 = "large format" }
						}
						
						// add to description text
						description_text += slide_format_1;
						if (slide_format_check_num == 2){
							description_text += " and " + slide_format_2;
						}
					}
					
				}	
				
				// Add closing bracket, if multiple photo types selected
				if ( num_photo_types > 1 ){
					description_text += ")"
				}
				
			}
			
			description.text(description_text); // Write the description text to the page
		};
		
		// Description function for Graphic Materials > Photograph > Album
		function gm_ph_a_set_description(){
			var description_text = "";
			var num_albums = parseInt(gm_ph_a.val(), 10); // number of albums
			var num_photos = parseInt(gm_ph_a_num.val(), 10); // number of photos
			var num_photos_aprx = gm_ph_a_num_num_aprx.filter( ":checked" ).val(); // is photo num approx?
			var album_cha = gm_ph_a_num_num.filter( ":checked" ).val(); // album colour characteristics
			var photo_size_num = gm_ph_a_num_num_cha.filter( ":checked" ).val(); // photo size number
			var photo_width1 = gm_ph_a_num_num_cha_width1.val(); // largest photo width
			var photo_height1 = gm_ph_a_num_num_cha_height1.val(); // largest photo height
			var photo_width2 = gm_ph_a_num_num_cha_width2.val(); // smaller photo width
			var photo_height2 = gm_ph_a_num_num_cha_height2.val(); // smaller photo height
			
			description_text += num_albums + " album";
			if (num_albums > 1){description_text += "s";}
			
			if (num_photos){
				description_text += " (";
				if (num_photos_aprx == "Yes"){ description_text += "ca. "; }// Is the number approximate?
				description_text += num_photos + " photograph";
				if (num_photos > 1){ description_text += "s"; }
				
				// Is Colour Characteristics selected?
				if (album_cha){
					if (album_cha == "Black & White"){ description_text += " : b&w"; }
					else if (album_cha == "Colour"){ description_text += " : col."; }
					else if (album_cha == "Both"){ description_text += " : b&w and col."; }
					
					// Is amount of protograph size dimensions indicated?
					if (photo_size_num){
						// Are photo dimensions provided?
						if (photo_width1 && photo_height1){
							if (photo_size_num == "1"){ 
								description_text += " ; "+photo_height1+" x "+photo_width1+" cm";  
							}
							else if (photo_size_num == "2" && photo_width2 && photo_height2){
								description_text += " ; "+photo_height1+" x "+photo_width1+" cm and "+photo_height2+" x "+photo_width2+" cm";
							}
							else if (photo_size_num == "3 or more"){
								description_text += " ; "+photo_height1+" x "+photo_width1+" cm or smaller";  
							}
						}	
					}					
				}
				
				description_text += ")";
			}
			
			description.text(description_text); // Write the description text to the page
		};
		
		// Description function for Graphic Materials > Print (Non-Photographic)
		function gm_pr_set_description(){
			var description_text = "";
			var num_pr = gm_pr.val(); // number of prints
			var num_pr_aprx = gm_pr_num_aprx.filter( ":checked" ).val(); // is print num approx?
			var pr_type = gm_pr_num.filter( ":checked" ).val(); // which 'Type'?
			var pr_cha = gm_pr_num_type.filter( ":checked" ).val(); // Colour Characteristics?
			var pr_proc = gm_pr_num_type_cha.filter( ":checked" ); // Which Process(es)?
			var pr_proc_unknown_check = gm_pr_num_type_cha.filter('[value=Unknown]').prop("checked"); // is 'Unknown' checked?
			var pr_size_num = gm_pr_num_type_cha_proc.filter( ":checked" ).val(); // # of size dimensions
			var width1 = gm_pr_num_type_cha_proc_width1.val();
			var height1 = gm_pr_num_type_cha_proc_height1.val();
			var width2 = gm_pr_num_type_cha_proc_width2.val();
			var height2 = gm_pr_num_type_cha_proc_height2.val();
			var pr_mnt = gm_pr_num_type_cha_proc_size.filter( ":checked" ).val(); // mount type
			var mnt_width = gm_pr_num_type_cha_proc_size_width.val();
			var mnt_height = gm_pr_num_type_cha_proc_size_height.val();
			
			if ( num_pr_aprx == "Yes" ){
				description_text += "ca. ";
			}
			description_text += num_pr + " print";
			if ( num_pr > 1 ){ description_text += "s"; }
			
			// Is a Type selected?
			if ( pr_type ){
				description_text += " : ";
				
				// Is a Colour Characteristic selected?
				if ( pr_cha ){
					if ( pr_cha == "Black & White" ){
						description_text+="b&w";
					}
					else if ( pr_cha == "Colour" ){
						description_text+="col.";
					}
					else if ( pr_cha == "Both" ){
						description_text+="b&w and col.";
					}
					
					// Is a Process selected
					if ( pr_proc.length > 0 ){
						// Make sure 'Unknown' process isn't checked
						if ( !(pr_proc_unknown_check) ){
							description_text += " ";
							
							// Function to add the 'Process' values programmatically
							pr_proc.each( function(i) {
								// first medium
								if ( i == 0 ){
									description_text += this.value.toLowerCase();
								}
								// second medium
								else if ( i == 1 ){
									if ( pr_proc.length == 2 ){
										description_text += " and ";
									}
									else{
										description_text += ", ";
									}
									description_text += this.value.toLowerCase();
								}
								// third medium
								else if ( i == 2 ){
									description_text += ", and " + this.value.toLowerCase();
								}
							});
						}

						// add Type, as long as it's not Unknown
						if ( pr_type != "Unknown" ){
							description_text += " " + pr_type.toLowerCase();
							if ( num_pr > 1 ){ description_text += "s"; }
						}
						
						// Are size dimensions provided?
						if ( pr_size_num ){
							if ( width1 && height1 ){
								if ( pr_size_num == "1"){
									description_text += " ; "+height1+" x "+width1+" cm";  
								}
								else if ( pr_size_num == "2" && width2 && height2 ){
									description_text+=" ; "+height1+" x "+width1+" cm and "+height2+" x "+width2+" cm";
								}
								else if ( pr_size_num == "3 or more" ){
									description_text += " ; "+height1+" x "+width1+" cm or smaller";  
								}
							}
							
							// Is mount type provided?
							if ( pr_mnt ){
								if ( pr_mnt.match("Sheet|Card|Mat") ){
									description_text += " on "
								}
								else if ( pr_mnt.match("Frame|Case") ){
									description_text += " in "
								}
								description_text+=pr_mnt.toLowerCase();
								
								// Are mount dimentions provided?
								if ( mnt_width && mnt_height ){
									description_text += " "+mnt_height+" x "+mnt_width+" cm";  
								}
							}	
						}
					}
					else{
						// add Type, as long as it's not Unknown
						// This should only trigger if Process is not selected yet
						if ( pr_type != "Unknown" ){
							description_text += " " + pr_type.toLowerCase();
							if ( num_pr > 1 ){ description_text += "s"; }
						}
					}
				}
				else{					
					// add Type, as long as it's not Unknown
					// This should only trigger if Colour Characterstic is not selected yet
					if ( pr_type != "Unknown" ){
						description_text += " " + pr_type.toLowerCase();
						if ( num_pr > 1 ){ description_text += "s"; }
					}
				}
			}
			
			description.text(description_text); // Write the description text to the page
		}
		
		// Description function for Cartographic Materials > Map, and Architectural and Technical Drawings
		function mat_set_description(){
			var description_text = "";
			var item_name = "";
			var cm_selected = cm.filter(":checked").val(); // Is a value picked for Cartographic Material?
			var atd_selected = atd.filter(":checked").val(); // Is a value picked for Arch/Tech Drawing?
			var num = mat.val(); // How many items?
			var num_aprx = mat_num_aprx.filter( ":checked" ).val(); // Is num of items approx?
			var sheets = mat_num.filter( ":checked" ).val(); // Multiple sheets?
			var sheets_num = mat_num_ms_num.val(); // # of sheets?
			var overlays = mat_num_ms.filter( ":checked" ).val(); // Overlays?
			var base_num = mat_num_ms_o_bmnum.val(); // # of base items
			var overlay_num = mat_num_ms_o_onum.val(); // # of overlays
			var bothsides = mat_num_ms_o.filter( ":checked" ).val(); // Printing on both sides?
			var mop = mat_num_ms_o_bs.filter( ":checked" ); // Method(s) of Production
			var repro_type = mat_num_ms_o_bs_r.filter( ":checked" ).val(); // Determine reproduction type?
			var repro_lc = mat_num_ms_o_bs_r_y.filter( ":checked" ).val(); // Reproduction line colour
			var repro_gc = mat_num_ms_o_bs_r_y_lc.filter( ":checked" ).val(); // Reproduction ground colour
			var process_w_bg = mat_num_ms_o_bs_r_y_w_bg.filter( ":checked" ).val(); // Process for white on black/grey
			var process_b_w = mat_num_ms_o_bs_r_y_b_w.filter( ":checked" ).val(); // Process for blue on white
			var process_db_cw = mat_num_ms_o_bs_r_y_db_cw.filter( ":checked" ).val(); // Process for dark brown on clean white
			var process_lb = mat_num_ms_o_bs_r_y_lb.filter( ":checked" ).val(); // Process for light brown
			var process_p_cw = mat_num_ms_o_bs_r_y_p_cw.filter( ":checked" ).val(); // Process for purple on clean white
			var process_pro = mat_num_ms_o_bs_r_y_pro.filter( ":checked" ).val(); // Process for pink/red/orange
			var repro_bg_w = mat_num_ms_o_bs_r_y_bg_wdw.filter( ":checked" ).val(); // Reproduction is black/gray on white
			var process_bg_w_g = mat_num_ms_o_bs_r_y_bg_wdw_g.filter( ":checked" ).val(); // Process for black/grey on white on glossy
			var process_bg_w_fv = mat_num_ms_o_bs_r_y_bg_wdw_p.filter( ":checked" ).val(); // Process for black/grey on white on fibres visible
			var process_gy = mat_num_ms_o_bs_r_y_gy.filter( ":checked" ).val(); // Process for green/yellow
			var process_m = mat_num_ms_o_bs_r_y_mc.filter( ":checked" ).val(); // Process for multiple colours
			var coloured = mat_num_ms_o_bs_mp.filter( ":checked" ).val(); // Coloured or partly coloured?
			var annotated = mat_num_ms_o_bs_mp_c.filter( ":checked" ).val(); // Annotated?
			var material = mat_num_ms_o_bs_mp_c_a.filter( ":checked" ).val(); // Material?
			var mounted = mat_num_ms_o_bs_mp_c_a_m.filter( ":checked" ).val(); // Mounted?
			var size_amt = mat_num_ms_o_bs_mp_c_a_m_m.filter( ":checked" ).val(); // # of sizes?
			var width1 = mat_num_ms_o_bs_mp_c_a_m_m_width1.val(); // width 1
			var width2 = mat_num_ms_o_bs_mp_c_a_m_m_width2.val(); // width 2
			var height1 = mat_num_ms_o_bs_mp_c_a_m_m_height1.val(); // height 1
			var height2 = mat_num_ms_o_bs_mp_c_a_m_m_height2.val(); // height 2
			
			// Are we describing a map, architectural drawing or technical drawing?
			if ( cm_selected ){
				item_name = "map";
			}
			else if ( atd_selected == "Architectural drawing" ){
				item_name = "architectural drawing";
			}
			else if ( atd_selected == "Technical drawing" ){
				item_name = "technical drawing";
			}
			
			// # of items provided?
			if ( num ){
				// Is the number approximate?
				if ( num_aprx == "Yes" ){ description_text += "ca. "; }
				description_text += num + " " + item_name;
				if ( num > 1 ){ description_text += "s"; }
				
				// Multiple sheets?
				if ( sheets == "Yes" && sheets_num ){
					description_text += " on " + sheets_num + " sheets";
				}
					
				// Overlays?
				if ( overlays == "Yes" && overlay_num ){
					description_text += " (";
					if ( base_num ){
						if ( base_num == 1 ) { description_text += base_num + " base, "; }
						else if ( base_num > 1 ) { description_text += base_num + " bases, "; }
					}
					if ( overlay_num == 1 ) { description_text += overlay_num + " overlay)"; }
					else if ( overlay_num > 1 ) { description_text += overlay_num + " overlays)"; }
				}
					
				// Both sides?
				if ( bothsides == "Yes" ){
					description_text += " : both sides";
				}
				
				// Method(s) of Production?
				if ( mop && mop.length > 0 ){
					// Function to add 'Method of Production' values
					mop.each( function(i) {
						// Insert spacers as appropriate
						if ( i == 0 && bothsides == "No" && ( mop.length > 1 || ( mop.length == 1 && !( this.value.match("Reproduction|Printing") ) ) ) ){
							description_text += " : ";
						}
						else if ( i == 0 && bothsides == "Yes" && ( mop.length > 1 || ( mop.length == 1 && !( this.value.match("Reproduction|Printing") ) ) ) ){
							description_text += ", ";
						}
						else if ( i == 1 ){
							if ( mop.length == 2 ){ description_text += " and "; }
							else{ description_text += ", "; }
						}
						else if ( i == 2 ){	description_text += ", and "; }
						
						// Insert appropriate 'Method of Production' text
						if ( this.value == "Hand-drawn" ){
							if ( num == 1 ){ description_text += "ms."; }
							else { description_text += "mss."; }
						}
						else if ( this.value == "Computer printout" ){
							description_text += "computer printout";
							if ( num > 1 ){ description_text += "s"; }
						}
						else if ( this.value == "Reproduction" && mop.length > 1 ){
							description_text += "reproductions";
						}
						else if ( this.value == "Printing" && mop.length > 1 ){
							description_text += "printed " + item_name + "s";
						}
					});
					
					// Reproduction process
					if ( repro_type == "No - It's a Xerox photocopy" ){
						// insert colon if needed
						if ( bothsides == "No" ){ description_text += " :"; }
						// insert comma if needed
						if ( !(description_text.slice(-1).match(":|p| ")) ){ description_text += ", "; }
						
						description_text += " photocopy";
					}
					else if ( repro_type == "Yes" && repro_lc && ( repro_gc || process_w_bg || process_b_w || process_db_cw || process_lb || process_p_cw || process_pro || process_bg_w_g || process_bg_w_fv || process_gy || process_m ) ){
						// insert colon if needed
						if ( bothsides == "No" ){ description_text += " : "; }
						// insert comma if needed
						if ( !(description_text.slice(-1).match(":|p| ")) ){ description_text += ", "; }
						
						// Determine exact production process
						if ( repro_gc == "Green" ){
							description_text += "aniline print";
						}
						else if ( repro_lc == "White" && repro_gc == "Blue" ){
							description_text += "blueprint";
						}
						else if ( ( process_b_w && process_b_w.match("Diazotype") ) || repro_gc == "Flecked White" || ( process_lb && process_lb.match("Diazotype") ) || ( process_pro && process_pro.match("Diazotype") ) || ( process_bg_w_fv && process_bg_w_fv.match("Diazotype") ) || ( process_gy && process_gy.match("Diazotype") ) ){
							description_text += "diazotype";
						}
						else if ( process_w_bg == "Electrofax" || ( repro_bg_w && repro_bg_w.match("Smooth") ) ){
							description_text += "Electrofax print";
						}
						else if ( ( process_lb && process_lb.match("Ferrogallic") ) || ( process_bg_w_fv && process_bg_w_fv.match("Ferrogallic") ) ){
							description_text += "ferrogallic print";
						}
						else if ( process_b_w == "Get-lithograph" || process_db_cw == "Get-lithograph" || process_lb == "Get-lithograph" || process_p_cw == "Gel-lithograph" || process_pro == "Gel-lithograph" || process_bg_w_fv == "Gel-lithograph" || process_gy == "Gel-lithograph" || process_m == "Gel-lithograph" ){
							description_text += "gel-lithograph print";
						}
						else if ( process_b_w == "Hectograph (Handmade)" || process_db_cw == "Hectograph (Handmade)" || process_lb == "Hectograph (handmade)" || process_p_cw == "Hectograph" || process_pro == "Hectograph" || process_bg_w_fv == "Hectograph (Handmade)" || process_gy == "Hectograph" || process_m == "Hectograph" ){
							description_text += "hectograph";
						}
						else if ( process_bg_w_fv && process_bg_w_fv.match("Stencil") ){
							description_text += "mimeograph";
						}
						else if ( process_w_bg && process_w_bg.match("Photostat") ){
							description_text += "negative Photostat print";
						}
						else if ( repro_lc == "White" && repro_gc == "Brown" ){
							description_text += "negative Van Dyke print";
						}
						else if ( process_b_w && process_b_w.match("Pellet") ){
							description_text += "Pellet print";
						}
						else if ( process_w_bg == "Photocopy (Xerox)" || process_bg_w_fv == "Photocopy (Xerox)" ){
							description_text += "photocopy";
						}
						else if ( process_bg_w_g && process_bg_w_g.match("Photostat") ){
							description_text += "positive Photostat print";
						}
						else if ( process_db_cw && process_db_cw.match("Van Dyke") ){
							description_text += "positive Van Dyke print";
						}
						else if ( repro_gc && repro_gc.match("Mottled") ){
							description_text += "sepia diazo print";
						}
						else if ( process_bg_w_g && process_bg_w_g.match("Halide") ){
							description_text += "silver halide print";
						}
						else if ( repro_gc == "Buff to Orange" ){
							description_text += "thermofax print";
						}
						else if ( process_bg_w_fv && process_bg_w_fv.match("Wash-off") ){
							description_text += "wash-off print";
						}
					}
				}
				
				// Coloured?
				if ( coloured ){
					// insert colon if needed
					if ( coloured != "No" && (!(repro_type) || repro_type == "No") && mop.length == 1 && mop.val().match("Reproduction|Printing") && bothsides == "No" ){
						description_text += " :";
					}
					// insert comma if needed
					if ( coloured != "No" && description_text.slice(-1) != ":" ){ description_text += ","; }
					
					if ( coloured == "Yes - All" ){
						description_text += " col.";
					}
					else if ( coloured == "Yes - Some" ){
						description_text += " some col.";
					}
					else if ( coloured == "Yes - All hand coloured" ){
						description_text += " hand col.";
					}
					else if ( coloured == "Yes - All coloured, with some hand coloured" ){
						description_text += " col. (some hand col.)";
					}
				}
				
				// Annotated?
				if ( annotated == "Yes" ){
					// insert colon if needed
					if ( coloured == "No" && (!(repro_type) || repro_type == "No") && mop.length == 1 && mop.val().match("Reproduction|Printing") && bothsides == "No" ){
						description_text += " :";
					}
					// insert comma if needed
					if ( description_text.slice(-1) != ":" ){ description_text += ","; }
					
					description_text += " annotations";
				}
				
				// Material
				if ( material && material != "Paper" ){
					// insert colon if needed
					if ( annotated == "No" && coloured == "No" && (!(repro_type) || repro_type == "No") && mop.length == 1 && mop.val().match("Reproduction|Printing") && bothsides == "No" ){
						description_text += " :";
					}
					// insert comma if needed
					if ( description_text.slice(-1) != ":" ){ description_text += ", "; }
					
					description_text += " on " + material.toLowerCase();
				}
				
				// Mounted?
				if ( mounted && mounted != "No" ){
					// insert colon if needed
					if ( material == "Paper" && annotated == "No" && coloured == "No" && (!(repro_type) || repro_type == "No") && mop.length == 1 && mop.val().match("Reproduction|Printing") && bothsides == "No" ){
						description_text += " :";
					}
					// insert comma if needed
					if ( description_text.slice(-1) != ":" ){ description_text += ","; }
					
					description_text += " mounted on ";
					
					if ( mounted.match("linen") ){
						description_text += "linen";
					}
					else if ( mounted.match("muslin") ){
						description_text += "muslin";
					}
					else if ( mounted.match("rods") ){
						description_text += "rods";
					}
				}
				
				// Size dimensions
				if ( size_amt ){
					if ( width1 && height1){
						if ( size_amt == "1" ){
							description_text += " ; "+height1+" x "+width1+" cm"; 
						}
						else if ( size_amt == "2" && width2 && height2 ){
							description_text += " ; "+height1+" x "+width1+" cm and "+height2+" x "+width2+" cm";
						}
						else if ( size_amt == "3 or more" ){
							description_text += " ; "+height1+" x "+width1+" cm or smaller";  
						}
					}
				}
				
			}
			
			description.text(description_text); // Write the description text to the page
		}
		
		
		// Description function for Cartographic Materials > Remote-Sensing Image
		function cm_rsi_set_description(){
			var description_text = "";
			var type = cm_rsi.filter( ":checked" ).val(); // what type of RSI?
			var pg_num = cm_rsi_pg.val(); // How many Photo Prints/Glass Negatives?
			var pg_num_aprx = cm_rsi_pg_num_aprx.filter( ":checked" ).val(); // Is num of photo/glass approx?
			var pg_size_num = cm_rsi_pg_num.filter(" :checked" ).val(); // how many sizes of photo/glass?
			var pg_width1 = cm_rsi_pg_num_amt_width1.val(); // photo/glass width 1
			var pg_width2 = cm_rsi_pg_num_amt_width2.val(); // photo/glass width 2
			var pg_height1 = cm_rsi_pg_num_amt_height1.val(); // photo/glass height 1
			var pg_height2 = cm_rsi_pg_num_amt_height2.val(); // photo/glass height 2
			var n_num = cm_rsi_n.val(); // How many Negative Reels?
			var n_num_aprx = cm_rsi_n_num_aprx.filter( ":checked" ).val(); // is num of reels approx?
			var n_count = cm_rsi_n_num.filter( ":checked" ).val(); // include image frame count?
			var n_count_num = parseInt(cm_rsi_n_num_cnt.val(), 10); // reel image frame count
			var n_count_num_aprx = cm_rsi_n_num_cnt_num_aprx.filter( ":checked" ).val(); // is reel image frame count approx?
			var n_dia = cm_rsi_n_num_cnt_dia.val(); // reel diameter
			
			// Is the number approximate?
			if ( ( pg_num_aprx == "Yes" && type.match("Photographic|Glass") ) || ( n_num_aprx == "Yes" && type.match("Negative") ) ){
				description_text += "ca. ";
			}
			
			// Type selected is 'Photographic print' or 'Glass negatives'
			if ( type.match("Photographic|Glass") ){
				// Is a number of images provided?
				if ( pg_num > 0 ){
					description_text += pg_num + " remote-sensing image"
					if ( pg_num > 1 ){ description_text += "s"; }
					// Type is 'Glass negatives'
					if ( type == "Glass negatives" ){
						description_text += " : glass negative";
						if ( pg_num > 1 ){ description_text += "s"; }
					}
					// Are size dimensions provided?
					if ( pg_size_num ){
						if ( pg_width1 && pg_height1 ){
							if ( pg_size_num == "1"){
								description_text += " ; "+pg_height1+" x "+pg_width1+" cm";  
							}
							else if ( pg_size_num == "2" && pg_width2 && pg_height2 ){
								description_text+=" ; "+pg_height1+" x "+pg_width1+" cm and "+pg_height2+" x "+pg_width2+" cm";
							}
							else if ( pg_size_num == "3 or more" ){
								description_text += " ; "+pg_height1+" x "+pg_width1+" cm or smaller";  
							}
						}
					}
				}
			}
			// Type selected is 'Negative reels'
			else{
				// Is a number of reels provided?
				if ( n_num > 0 ){
					description_text += n_num + " remote-sensing reel";
					if ( n_num > 1 ){ description_text += "s"; }
					// Include count of images frames?
					if ( n_count == "Yes" && n_count_num > 0 ){
						description_text += " (";
						// Is the count approximate?
						if ( n_count_num_aprx == "Yes" ){
							description_text += "ca. ";
						}
						description_text += n_count_num.toLocaleString() + " fr.)";
					}
					description_text += " : negative";
					// Reel diameter
					if ( n_dia > 0 ){
						description_text += " ; "+n_dia+" cm diam.";
					}
				}
			}
			
			description.text(description_text); // Write the description text to the page
		}
		
		
		//
		// The functions below are for hiding/unhiding form elements, and getting values from inputs
		//
		
		// Primary menu (What are you describing?)
		desc_type.click(function(){
			var value=this.value;						
			all.addClass('hidden'); // hide everything and reveal as needed
			
			show=desc_type_parent // reset the show variable
			
			//description.text( "Value.clickd" ); //debugging
			
			if (value == 'Textual Records'){
				description.text( "Placeholder: Textual Records" );
				tr.val(''); // clear value of next form element
				tr_parent.removeClass('hidden'); // unhide next form element
			}
			else if (value == 'Graphic Materials'){
				description.text( "Placeholder: Graphic Materials" );	
				gm.prop( 'checked', false ); // clear values of next form element
				gm_parent.removeClass('hidden'); // unhide next form element
			}		
			else if (value == 'Cartographic Materials'){
				description.text( "Placeholder: Cartographic Materials" );
				cm.prop( 'checked', false ); // clear values of next form element
				atd.prop( 'checked', false ); // clear values of next Arch/Tech Drawings form element
				cm_parent.removeClass('hidden'); // unhide next form element
				
			}
			else if (value == 'Architectural and Technical Drawings'){
				description.text( "Placeholder: Architectural and Technical Drawings" );
				atd.prop( 'checked', false ); // clear values of next form element
				cm.prop( 'checked', false ); // clear values of next Cartographic Materials form element
				atd_parent.removeClass('hidden'); // unhide next form element
			}
			
			show.removeClass('hidden');
		});	
		
		// Textual Records > [Linear ext]
		tr.on( 'input', function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything and reveal as needed
			show=show.add(tr_parent); // add current form element
			
			if (value){
				show=show.add(tr_le_parent); // add next form element to show variable
			}
			else{
				show=desc_type_parent.add(tr_parent);
				tr_le.prop( 'checked', false); // remove value on 'cm or m' form element
				tr_le_m.prop( 'checked', false); // remove value on 'more granular' form element
			}
			
			show.removeClass('hidden');
			tr_set_description(); // update the RAD description
		});
		
		// Textual Records > Linear ext > [length]
		tr_le.click(function(event){
			var value=this.value;
			var linear_ext = parseInt(tr.val(), 10); 
			
			if (value == "cm" && linear_ext > 50){
				this.checked = false;
				//event.preventDefault(); // prevent the cm radio button from being selected
			}
			else if (value){
				show=show.add(tr_le_m_parent); // add next form element to show variable
			}

			show.removeClass('hidden');
			tr_set_description(); // update the RAD description
		});
		
		// Textual Records > Linear ext > Length > [Granular]
		tr_le_m.click(function(){
			var value=this.value;
			if (value){
				show=show.add(tr_le_m_gm_parent); // add next form element to show variable
			}
			
			show.removeClass('hidden');
			tr_set_description(); // update the RAD description
		});
		
		// Textual Records > Linear ext > Length > Granular > [Amount]
		tr_le_m_gm.on( 'input', function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything and reveal as needed
			
			if (value){
				show=show.add(tr_le_m_gm_amt_parent); // add next form element to show variable
			}
			else{
				show=desc_type_parent.add(tr_parent).add(tr_le_parent).add(tr_le_m_parent).add(tr_le_m_gm_parent); // reset the show variable
				tr_le_m_gm_amt.prop( 'checked', false); // reset the 'size dimensions' radio button
			}
			
			show.removeClass('hidden');
			tr_set_description(); // update the RAD description
		});
		
		// Textual Records > Linear ext > Length > Granular > Amount > [# of sizes]
		tr_le_m_gm_amt.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything and reveal as needed
			
			if (value){
				show=show.add(tr_le_m_gm_amt_width1_parent).add(tr_le_m_gm_amt_height1_parent);
				if (value == "2"){
					show=show.add(tr_le_m_gm_amt_width2_parent).add(tr_le_m_gm_amt_height2_parent);
				}
				else{
					show=show.not(tr_le_m_gm_amt_width2_parent).not(tr_le_m_gm_amt_height2_parent);
				}
			}
			
			show.removeClass('hidden');
			tr_set_description(); // update the RAD description
		});
		
		// Textual Records > Linear ext > Length > Granular > Amount > # of sizes > [Width 1]
		tr_le_m_gm_amt_width1.on( 'input', function(){
			tr_set_description();
		});
		
		// Textual Records > Linear ext > Length > Granular > Amount > # of sizes > [Height 1]
		tr_le_m_gm_amt_height1.on( 'input', function(){
			tr_set_description();
		});
		
		// Textual Records > Linear ext > Length > Granular > Amount > # of sizes > [Width 2]
		tr_le_m_gm_amt_width2.on( 'input', function(){
			tr_set_description();
		});
		
		// Textual Records > Linear ext > Length > Granular > Amount > # of sizes > [Height 2]
		tr_le_m_gm_amt_height2.on( 'input', function(){
			tr_set_description();
		});
		
		// Graphic Materials > [Specific Material]
		gm.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			var show = gm_parent;
			show.removeClass('hidden');
			
			if (value == "Collage"){
				gm_c.val(''); // clear value from next form element
				gm_c_parent.removeClass('hidden'); // unhide next form element
			}
			else if (value == "Drawing"){
				gm_d.val(''); // clear value from next form element
				gm_d_parent.removeClass('hidden'); // unhide 'how many drawings' element
			}
			else if (value == "Painting"){
				gm_p.val(''); // clear value from next form element
				gm_p_parent.removeClass('hidden');
			}
			else if (value == "Photograph"){
				gm_ph.prop( 'checked', false ); // clear values of next form element
				gm_ph_parent.removeClass('hidden'); // unhide next form element
			}
			else if (value == "Print (Non-Photographic)"){
				gm_pr.prop( 'checked', false ); // clear value of next form element
				gm_pr_parent.removeClass('hidden'); // unhide next form element
			}
		});
		
		// Graphic Materials > Collage > [How many]
		gm_c.on('input', function(){
			var value=parseInt(this.value, 10);
			
			all.addClass('hidden'); // hide everything
			show = gm_parent.add(gm_c_parent);
			
			if ( value ){
				show = show.add(gm_c_num_parent); // add 'medium' element 
				if ( value > 1 ){
					show = show.add(gm_c_num_aprx_parent); // add 'is approximation' element 
				}
				else{
					show = show.not(gm_c_num_aprx_parent); // remove 'is approximation' element 
					gm_c_num_aprx.prop( 'checked', false); // clear 'is approximation' radio button
				}
			}
			else{
				show = show.not(gm_c_num_parent); // remove 'medium' element 
				gm_c_num.prop( 'checked', false); // clear 'medium' radio button
				show = show.not(gm_c_num_aprx_parent); // remove 'is approximation' element 
				gm_c_num_aprx.prop( 'checked', false); // clear 'is approximation' radio button
			}
			
			show.removeClass('hidden');
			gm_c_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Collage > How many > [Apprx]
		gm_c_num_aprx.click(function(){
			gm_c_set_description(); // Update the RAD Description
		});
				
		// Graphic Materials > Collage > How many > [Medium]
		gm_c_num.click(function(){
			var value=this.value;
			var num_med=gm_c_num.filter(":checked").length; // # checked in 'medium'
			var unknown_check=gm_c_num.filter('[value=Unknown]').prop("checked"); // is 'Unknown' checked?
			
			all.addClass('hidden'); // hide everything
			
			
			if ( ( num_med > 3 ) || ( num_med > 1 && ( value == "Unknown" || unknown_check ) ) ){
				// Make sure no more than 3 checked, and Unknown not checked in combination with anything else
				this.checked = false;
			}
			else if ( num_med ){
				show=show.add(gm_c_num_med_parent); // add 'size dimensions' element 
			}
			else{
				show=show.not(gm_c_num_med_parent); // remove 'size dimensions' element 
			}
			
			show.removeClass('hidden');
			gm_c_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Collage > How many > Medium > [# of sizes]
		gm_c_num_med.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if (value){
				show=show.add(gm_c_num_med_width1_parent).add(gm_c_num_med_height1_parent);
				if ( value == "2" ){
					show=show.add(gm_c_num_med_width2_parent).add(gm_c_num_med_height2_parent);
				}
				else{
					show=show.not(gm_c_num_med_width2_parent).not(gm_c_num_med_height2_parent);
				}
			}
			else{
				show=show.not(gm_c_num_med_width1_parent).not(gm_c_num_med_height1_parent);
				show=show.not(gm_c_num_med_width2_parent).not(gm_c_num_med_height2_parent);
			}
			
			show.removeClass('hidden');
			gm_c_set_description(); // Update the RAD Description
		});
		
		
		// Graphic Materials > Collage > How many > Medium > # of sizes > [Width 1]
		gm_c_num_med_width1.on( 'input', function(){
			var width1=this.value;
			var height1 = gm_c_num_med_height1.val();
			
			all.addClass('hidden'); // hide everything
			
			if ( width1 && height1 ){
				show=show.add(gm_c_num_cha_med_size_parent); // add 'mount' element 
			}
			else{
				show=show.not(gm_c_num_cha_med_size_parent); // remove 'mount' element 
			}
			
			show.removeClass('hidden');
			gm_c_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Collage > How many > Medium > # of sizes > [Height 1]
		gm_c_num_med_height1.on( 'input', function(){
			var height1=this.value;
			var width1 = gm_c_num_med_width1.val();
			
			all.addClass('hidden'); // hide everything
			
			if ( width1 && height1 ){
				show=show.add(gm_c_num_cha_med_size_parent); // add 'mount' element 
			}
			else{
				show=show.not(gm_c_num_cha_med_size_parent); // remove 'mount' element 
			}
			
			show.removeClass('hidden');
			gm_c_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Collage > How many > Medium > # of sizes > [Width 2]
		gm_c_num_med_width2.on( 'input', function(){
			gm_c_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Collage > How many > Medium > # of sizes > [Height 2]
		gm_c_num_med_height2.on( 'input', function(){
			gm_c_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Collage > How many > Medium > # of sizes > Size > [Mount]
		gm_c_num_cha_med_size.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if (value){
				show=show.add(gm_c_num_cha_med_size_width_parent).add(gm_c_num_cha_med_size_height_parent);
			}
			else{
				show=show.not(gm_c_num_cha_med_size_width_parent).not(gm_c_num_cha_med_size_height_parent);
			}
			
			show.removeClass('hidden');
			gm_c_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Collage > How many > Medium > # of sizes > Size > Mount > [Mount Width]
		gm_c_num_cha_med_size_width.on( 'input', function(){
			gm_c_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Collage > How many > Medium > # of sizes > Size > Mount > [Mount height]
		gm_c_num_cha_med_size_height.on( 'input', function(){
			gm_c_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Drawing > [How many]
		gm_d.on( 'input', function(){
			var value=parseInt(this.value, 10);
			
			all.addClass('hidden'); // hide everything
			show = gm_parent.add(gm_d_parent);
			
			if ( value ){
				show = show.add(gm_d_num_parent); // add 'type' element 
				if ( value > 1 ){
					show = show.add(gm_d_num_aprx_parent); // add 'is num apprx' element 
				}
				else{
					show = show.not(gm_d_num_aprx_parent); // remove 'is num apprx' element 
					gm_d_num_aprx.prop( 'checked', false); // cleare 'is num apprx' radio button
				}
			}
			else{
				show = show.not(gm_d_num_parent); // remove 'type' element 
				gm_d_num.prop( 'checked', false); // clear 'type' radio button
				show = show.not(gm_d_num_aprx_parent); // remove 'is num apprx' element 
				gm_d_num_aprx.prop( 'checked', false); // cleare 'is num apprx' radio button
			}
			
			show.removeClass('hidden');
			gm_d_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Drawing > How many > [Apprx]
		gm_d_num_aprx.click(function(){
			gm_d_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Drawing > How many > [Type]
		gm_d_num.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if ( value ){
				show=show.add(gm_d_num_type_parent); // add 'medium' element 
			}
			
			show.removeClass('hidden');
			gm_d_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Drawing > How many > Type > [Medium]
		gm_d_num_type.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if ( value ){
				show=show.add(gm_d_num_type_med_parent); // add '# of dimensions' element 
			}
			
			show.removeClass('hidden');
			gm_d_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Drawing > How many > Type > Medium > [# of sizes]
		gm_d_num_type_med.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if ( value ){
				show=show.add(gm_d_num_type_med_width1_parent).add(gm_d_num_type_med_height1_parent);
				if ( value == "2" ){
					show=show.add(gm_d_num_type_med_width2_parent).add(gm_d_num_type_med_height2_parent);
				}
				else{
					show=show.not(gm_d_num_type_med_width2_parent).not(gm_d_num_type_med_height2_parent);
				}
			}
			
			show.removeClass('hidden');
			gm_d_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Drawing > How many > Type > Medium > # of sizes > [Width 1]
		gm_d_num_type_med_width1.on( 'input', function(){
			var width1=this.value;
			var height1=gm_d_num_type_med_height1.val();
			
			all.addClass('hidden'); // hide everything
			
			if ( width1 && height1 ){
				show=show.add(gm_d_num_type_med_size_parent); // add 'mount' element 
			}
			else{
				show=show.not(gm_d_num_type_med_size_parent); // remove 'mount' element 
			}
			
			show.removeClass('hidden');
			gm_d_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Drawing > How many > Type > Medium > # of sizes > [Height 1]
		gm_d_num_type_med_height1.on( 'input', function(){
			var width1=gm_d_num_type_med_width1.val();
			var height1=this.value;
			
			all.addClass('hidden'); // hide everything
			
			if ( width1 && height1 ){
				show=show.add(gm_d_num_type_med_size_parent); // add 'mount' element 
			}
			else{
				show=show.not(gm_d_num_type_med_size_parent); // remove 'mount' element 
			}
			
			show.removeClass('hidden');
			gm_d_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Drawing > How many > Type > Medium > # of sizes > [Width 2]
		gm_d_num_type_med_width2.on( 'input', function(){
			gm_d_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Drawing > How many > Type > Medium > # of sizes > [Height 2]
		gm_d_num_type_med_height2.on( 'input', function(){
			gm_d_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Drawing > How many > Type > Medium > # of sizes > Sizes > [Mount]
		gm_d_num_type_med_size.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if ( value ){
				show=show.add(gm_d_num_type_med_size_width_parent).add(gm_d_num_type_med_size_height_parent);
			}
			
			show.removeClass('hidden');
			gm_d_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Drawing > How many > Type > Medium > # of sizes > Sizes > Mount > [Width]
		gm_d_num_type_med_size_width.on( 'input', function(){
			gm_d_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Drawing > How many > Type > Medium > # of sizes > Sizes > Mount > [Height]
		gm_d_num_type_med_size_height.on( 'input', function(){
			gm_d_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Painting > [How many]
		gm_p.on( 'input', function(){
			var value=parseInt(this.value, 10);
			
			all.addClass('hidden'); // hide everything
			show = gm_parent.add(gm_p_parent);
			
			if ( value ){
				show = show.add(gm_p_num_parent); // add 'type' element 
				if ( value > 1 ){
					show = show.add(gm_p_num_aprx_parent); // add 'is num apprx' element 
				}
				else{
					show = show.not(gm_p_num_aprx_parent); // remove 'is num apprx' element 
					gm_p_num_aprx.prop( 'checked', false); // clear 'is num apprx' radio button
				}
			}
			else{
				show = show.not(gm_p_num_parent); // add 'type' element 
				gm_p_num.prop( 'checked', false); // clear 'type' radio button
				show = show.not(gm_p_num_aprx_parent); // remove 'is num apprx' element 
				gm_p_num_aprx.prop( 'checked', false); // clear 'is num apprx' radio button
			}
			
			show.removeClass('hidden');
			gm_p_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Painting > How many > [Apprx]
		gm_p_num_aprx.click(function(){
			gm_p_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Painting > How many > [Type]
		gm_p_num.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if ( value ){
				show=show.add(gm_p_num_type_parent); // add 'medium' element 
			}
			
			show.removeClass('hidden');
			gm_p_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Painting > How many > Type > [Medium]
		gm_p_num_type.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if ( value ){
				if ( value == "Unknown" ){
					show=show.add(gm_p_num_type_med_base_parent); // add '# of sizes' element 
					show=show.not(gm_p_num_type_med_parent); // remove 'base' element 
					gm_p_num_type_med.prop( 'checked', false ); // clear value from 'base' element
				}
				else{
					show=show.add(gm_p_num_type_med_parent); // add 'base' element 
					show=show.not(gm_p_num_type_med_base_parent); // remove '# of sizes' element 
					gm_p_num_type_med_base.prop( 'checked', false ); // cleaer value from '# of sizes' element
				}
			}
			
			show.removeClass('hidden');
			gm_p_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Painting > How many > Type > Medium > [Base]
		gm_p_num_type_med.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if ( value ){
				show=show.add(gm_p_num_type_med_base_parent); // add '# of sizes' element 
			}
			
			show.removeClass('hidden');
			gm_p_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Painting > How many > Type > Medium > Base > [# of sizes]
		gm_p_num_type_med_base.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if ( value ){
				show=show.add(gm_p_num_type_med_base_width1_parent).add(gm_p_num_type_med_base_height1_parent);
				if ( value == "2" ){
					show=show.add(gm_p_num_type_med_base_width2_parent).add(gm_p_num_type_med_base_height2_parent);
				}
				else{
					show=show.not(gm_p_num_type_med_base_width2_parent).not(gm_p_num_type_med_base_height2_parent);
				}
			}
			
			show.removeClass('hidden');
			gm_p_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Painting > How many > Type > Medium > Base > # of sizes > [Width 1]
		gm_p_num_type_med_base_width1.on( 'input', function(){
			var width1=this.value;
			var height1=gm_p_num_type_med_base_height1.val();
			
			all.addClass('hidden'); // hide everything
			
			if ( width1 && height1 ){
				show=show.add(gm_p_num_type_med_base_size_parent); // add 'mount' element 
			}
			else{
				show=show.not(gm_p_num_type_med_base_size_parent); // remove 'mount' element 
			}
			
			show.removeClass('hidden');
			gm_p_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Painting > How many > Type > Medium > Base > # of sizes > [Height 1]
		gm_p_num_type_med_base_height1.on( 'input', function(){
			var width1=gm_p_num_type_med_base_width1.val();
			var height1=this.value;
			
			all.addClass('hidden'); // hide everything
			
			if ( width1 && height1 ){
				show=show.add(gm_p_num_type_med_base_size_parent); // add 'mount' element 
			}
			else{
				show=show.not(gm_p_num_type_med_base_size_parent); // remove 'mount' element 
			}
			
			show.removeClass('hidden');
			gm_p_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Painting > How many > Type > Medium > Base > # of sizes > [Width 2]
		gm_p_num_type_med_base_width2.on( 'input', function(){
			gm_p_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Painting > How many > Type > Medium > Base > # of sizes > [Height 2]
		gm_p_num_type_med_base_height2.on( 'input', function(){
			gm_p_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Painting > How many > Type > Medium > Base > # of sizes > [Mount]
		gm_p_num_type_med_base_size.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if ( value ){
				show=show.add(gm_p_num_type_med_base_size_width_parent).add(gm_p_num_type_med_base_size_height_parent);
			}
			
			show.removeClass('hidden');
			gm_p_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Painting > How many > Type > Medium > Base > # of sizes > Sizes > Mount > [Width]
		gm_p_num_type_med_base_size_width.on( 'input', function(){
			gm_p_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Painting > How many > Type > Medium > Base > # of sizes > Sizes > Mount > [Height]
		gm_p_num_type_med_base_size_height.on( 'input', function(){
			gm_p_set_description(); // Update the RAD Description
		});		
		
		// Graphic Materials > Photograph > [What]
		gm_ph.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			var show = gm_parent.add(gm_ph_parent);
			show.removeClass('hidden');
			
			if (value == "Photograph"){
				//gm_ph_sp.prop( 'checked', false ); //clear values of next form element
				gm_ph_ph_parent.removeClass('hidden'); //unhide next form element
				
			}
			else if (value == "Album"){
				description.text( "Placeholder Photograph Album" ); //RAD Physical Description
				//gm_ph_mp.val( '' ); //clear values of next form element
				gm_ph_a_parent.removeClass('hidden'); //unhide next form element
			}
		});
		
		// Graphic Materials > Photo > Photo(s) > [How many]
		gm_ph_ph.on('input', function(){
			var num_photos = parseInt(this.value, 10);
			
			all.addClass('hidden'); // hide everything
			show = gm_parent.add(gm_ph_parent).add(gm_ph_ph_parent); // using the global version
				
			if (num_photos){
				//gm_ph_mp_num.prop( 'checked', false ); //clear values of next form element
				gm_ph_ph_num_parent.removeClass('hidden'); //unhide next form element 'what type?'
			}
			
			if (num_photos > 1){
				show = show.add(gm_ph_ph_num_aprx_parent); //add "is this approximate?" to list of items to show
			}
			else{
				gm_ph_ph_num_aprx.prop( 'checked', false ); // clear values from "is this approximate?"
				gm_ph_ph_num.prop( 'checked', false ); // clear values from "type of photograph"?)
			}
			
			show.removeClass('hidden');
			
			gm_ph_ph_set_description(); // update the RAD description
		});
		
		// Graphic Materials > Photo > Photo(s) > How many > [Approximation?]
		gm_ph_ph_num_aprx.click(function(){
			//var value=this.value;
			//console.log(value);
			
			//all.addClass('hidden'); // hide everything
			//var show = gm_parent.add(gm_ph_parent).add(gm_ph_mp_parent).add(gm_ph_mp_num_parent);
			//show.removeClass('hidden');
			
			gm_ph_ph_set_description();
		});
		
		// Graphic Materials > Photo > Photo(s) > How many > [Type]
		gm_ph_ph_num.click(function(){
			var num_photos = parseInt(gm_ph_ph.val(), 10);
			//var value=this.value;
			// Prevent more than allowed number of boxes from being checked
			// 1 if number of photos = 1
			var num_checked = gm_ph_ph_num.filter(":checked").length;
			if ( num_photos == 1 && num_checked >1 ){
				//event.preventDefault(); // prevent this checkbox button from being selected
				this.checked = false;
			}
			else if ( num_photos == 2 && num_checked >2 ){
				//event.preventDefault(); // prevent this checkbox button from being selected
				this.checked = false;
			}
			
			// Determine which boxes are being checked
			var gm_ph_ph_num_check_p = gm_ph_ph_num.filter('[value=Print]').prop("checked"); //is 'Print(s)' checked?
			var gm_ph_ph_num_check_n = gm_ph_ph_num.filter('[value=Negative]').prop("checked"); //is 'Negative(s)' checked?
			var gm_ph_ph_num_check_s = gm_ph_ph_num.filter('[value=Slide]').prop("checked"); //is 'Slide(s)' checked?
			
			all.addClass('hidden'); // hide everything
			
			show = show.add(gm_ph_ph_num_parent); // still using the global version
			
			// Is Print checked, and how many photos?
			if ( gm_ph_ph_num_check_p ){
				show=show.add(gm_ph_ph_num_p_parent); // add 'colour characteristics' element 
				if ( num_checked > 1 ){
					show=show.add(gm_ph_ph_num_p_num_parent); // add 'how many prints' element 
				}
				else{
					show=show.not(gm_ph_ph_num_p_num_parent); // remove 'how many prints' element 
					gm_ph_ph_num_p_num.val(''); // clear the 'how many prints' text box
				}
			}
			else{
				show=show.not(gm_ph_ph_num_p_parent); // remove 'colour characteristics' element 
				gm_ph_ph_num_p.prop( 'checked', false ); // clear the 'colour characteristics' element
				show=show.not(gm_ph_ph_num_p_num_parent); // remove 'how many prints' element 
				gm_ph_ph_num_p_num.val(''); // clear the 'how many prints' text box
			}
			
			// Is Negative checked, and how many photos?
			if ( gm_ph_ph_num_check_n ){
				show=show.add(gm_ph_ph_num_n_parent); // add 'what is medium' element 
				//show = show.add(gm_ph_ph_num_n_parent); // add next form element 
				if ( num_checked > 1 ){
					show=show.add(gm_ph_ph_num_n_num_parent); // add 'how many negatives' element to show var
				}
				else{
					show=show.not(gm_ph_ph_num_n_num_parent); // remove 'how many negatives' element 
					gm_ph_ph_num_n_num.val(''); // clear the 'how many negatives' text box
				}
			}
			else{
				show=show.not(gm_ph_ph_num_n_parent); // remove 'what is medium' element 
				gm_ph_ph_num_n.prop( 'checked', false ); // clear 'what is medium' checkboxes
				show=show.not(gm_ph_ph_num_n_num_parent); // remove 'how many negatives' element from show var
				gm_ph_ph_num_n_num.val(''); // clear the 'how many negatives' text box
			}
			
			// Is Slide checked, and how many photos?
			if ( gm_ph_ph_num_check_s ){
				show = show.add(gm_ph_ph_num_s_num_parent); // add 'colour characteristics' element 
				if ( num_checked > 1 ){
					show = show.add(gm_ph_ph_num_s_parent); // add 'how many slides' element 
				}
				else{
					show = show.not(gm_ph_ph_num_s_parent); // remove 'how many slides' element 
					gm_ph_ph_num_s.val("");  // clear the 'how many slides' text box
				}
			}
			else{
				show = show.not(gm_ph_ph_num_s_parent); // remove 'how many slide(s)' form element 
				gm_ph_ph_num_s.val("");  // clear 'how many slide(s)' value
				show = show.not(gm_ph_ph_num_s_num_parent); // remove 'colour characteristics' element 
				gm_ph_ph_num_s_num.prop( 'checked', false ); // clear 'colour characteristics' checkboxes
				show = show.not(gm_ph_ph_num_s_num_cha); // remove 'slide format' form element 
				gm_ph_ph_num_s_num_cha.prop( 'checked', false ); // clear 'slide format' checkboxes
			}
			
			//console.log(show)
			show.removeClass('hidden');
			gm_ph_ph_set_description();
		});
		
		// Graphic Materials > Photo > Photo > How many > Print > [How many]
		gm_ph_ph_num_p_num.on( 'input', function(){
			gm_ph_ph_set_description();
		});
		
		// Graphic Materials > Photo > Photo > How many > Print > [Col Cha]
		gm_ph_ph_num_p.change(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if ( value ){
				show=show.add(gm_ph_ph_num_p_cha_parent); // add 'Type' element 
			}
			
			show.removeClass('hidden');
			gm_ph_ph_set_description();
		});
		
		// Graphic Materials > Photo > Photo > How many > Print > Col Cha > [Type]
		gm_ph_ph_num_p_cha.change(function(){
			var value=this.value;
			var col_cha=gm_ph_ph_num_p.filter(":checked").val();
			
			all.addClass('hidden'); // hide everything
			
			// Prevent from picking type that doesn't match colour characteristic
			if ( col_cha == "Black & White" && value.match("Carbro|Imbibation|Chromogenic|Cibachrome|Dye Diffusion") ){			
				this.checked = false;
				value="";
			}
			else if ( col_cha == "Colour" && value.match("Daguerr|Cyan|Ambro|Tintype|Albumen|Carbon|Platinum|Printing-Out|Dichromate|Gelatin|Collodion|Bromoil") ){				
				this.checked = false;
				value="";
			}
			
			if ( value ){
				show=show.add(gm_ph_ph_num_p_cha_typ_parent); // add 'how many size' element 
			}
			else{
				show=show.not(gm_ph_ph_num_p_cha_typ_parent); // remove 'how many size' element 
			}
			
			show.removeClass('hidden');
			gm_ph_ph_set_description();
		});
		
		// Graphic Materials > Photo > Photo > How many > Print > Col Cha > Type > [# of sizes]
		gm_ph_ph_num_p_cha_typ.change(function(){
			var value=this.value;			all.addClass('hidden'); // hide everything
			
			if ( value ){
				show=show.add(gm_ph_ph_num_p_cha_typ_width1_parent).add(gm_ph_ph_num_p_cha_typ_height1_parent);
				if ( value == "2" ){
					show=show.add(gm_ph_ph_num_p_cha_typ_width2_parent).add(gm_ph_ph_num_p_cha_typ_height2_parent);
				}
				else{
					show=show.not(gm_ph_ph_num_p_cha_typ_width2_parent).not(gm_ph_ph_num_p_cha_typ_height2_parent);
				}
			}
			
			show.removeClass('hidden');
			gm_ph_ph_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Photo > Photo > How many > Print > Col Cha > Type > # of sizes > [Width 1]
		gm_ph_ph_num_p_cha_typ_width1.on( 'input', function(){
			var width1=this.value;
			var height1=gm_ph_ph_num_p_cha_typ_height1.val();
			
			all.addClass('hidden'); // hide everything
			
			if ( width1 && height1 ){
				show=show.add(gm_ph_ph_num_p_cha_typ_size_parent); // add 'mount' element 
			}
			else{
				show=show.not(gm_ph_ph_num_p_cha_typ_size_parent); // remove 'mount' element 
			}
			
			show.removeClass('hidden');
			gm_ph_ph_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Photo > Photo > How many > Print > Col Cha > Type > # of sizes > [Height 1]
		gm_ph_ph_num_p_cha_typ_height1.on( 'input', function(){
			var width1=gm_ph_ph_num_p_cha_typ_width1.val();
			var height1=this.value;
			
			all.addClass('hidden'); // hide everything
			
			if ( width1 && height1 ){
				show=show.add(gm_ph_ph_num_p_cha_typ_size_parent); // add 'mount' element 
			}
			else{
				show=show.not(gm_ph_ph_num_p_cha_typ_size_parent); // remove 'mount' element 
			}
			
			show.removeClass('hidden');
			gm_ph_ph_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Photo > Photo > How many > Print > Col Cha > Type > # of sizes > [Width 2]
		gm_ph_ph_num_p_cha_typ_width2.on( 'input', function(){
			gm_ph_ph_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Photo > Photo > How many > Print > Col Cha > Type > # of sizes > [Height 2]
		gm_ph_ph_num_p_cha_typ_height2.on( 'input', function(){
			gm_ph_ph_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Photo > Photo > How many > Print > Col Cha > Type > # of sizes > [Mount]
		gm_ph_ph_num_p_cha_typ_size.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if ( value ){
				show=show.add(gm_ph_ph_num_p_cha_typ_size_width_parent).add(gm_ph_ph_num_p_cha_typ_size_height_parent);
			}
			else{
				show=show.not(gm_ph_ph_num_p_cha_typ_size_width_parent).not(gm_ph_ph_num_p_cha_typ_size_height_parent);
			}
			
			show.removeClass('hidden');
			gm_ph_ph_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Photo > Photo > How many > Print > Col Cha > Type > # of sizes > Mount > [Width]
		gm_ph_ph_num_p_cha_typ_size_width.on( 'input', function(){
			gm_ph_ph_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Photo > Photo > How many > Print > Col Cha > Type > # of sizes > Mount > [Height]
		gm_ph_ph_num_p_cha_typ_size_height.on( 'input', function(){
			gm_ph_ph_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Photo > Photo > How many > Negative > [How many]
		gm_ph_ph_num_n_num.on( 'input', function(){
			var num_neg = parseInt(this.value, 10);
			
			// logic here to make sure appropriate values are maintained, depending on amount of negatives. see equivalent function for number of slides
			
			gm_ph_ph_set_description();
		});
		
		// Graphic Materials > Photo > Photo > How many > Negative > [Medium]
		gm_ph_ph_num_n.click(function(){
			var film_check = gm_ph_ph_num_n.filter('[value=Film]').prop("checked"); // is 'Film' checked?
			var film_other_check = gm_ph_ph_num_n_f_cha.filter('[value=Other]').prop("checked"); // is 'Other' checked for Film Format?
			var glass_check = gm_ph_ph_num_n.filter('[value=Glass]').prop("checked"); // is 'Glass' checked?
			var glass_type_check = gm_ph_ph_num_n.prop("checked").length; // is a 'Glass Type' checked?
			
			all.addClass('hidden'); // hide everything
			
			// Hides/shows elements as appropriate, based on 'Film' checkbox
			if (film_check){
				show = show.add(gm_ph_ph_num_n_f_parent); //add 'colour characteristics' element 
			}
			else{
				show = show.not(gm_ph_ph_num_n_f_parent); // remove 'colour characteristics' element 
				gm_ph_ph_num_n_f.prop( 'checked', false); // clear the 'colour characteristics' checkboxes
				show = show.not(gm_ph_ph_num_n_f_b_parent); // remove 'nitrate' element 
				gm_ph_ph_num_n_f_b.prop( 'checked', false); // clear the 'nitrate' checkboxes
				show = show.not(gm_ph_ph_num_n_f_cha_parent); // remove 'film format' element 
				gm_ph_ph_num_n_f_cha.prop( 'checked', false); // clear the 'film format' checkboxes			
			}
			film_other_check = gm_ph_ph_num_n_f_cha.filter('[value=Other]').prop("checked"); // is 'Other' checked
			
			// Hides/shows elements as appropriate, based on 'Glass' checkbox
			if (glass_check){
				show = show.add(gm_ph_ph_num_n_g_parent); //add 'type of glass' element 
			}
			else{
				show = show.not(gm_ph_ph_num_n_g_parent); // remove 'type of glass' element 
				gm_ph_ph_num_n_g.prop( 'checked', false); // clear the 'type of glass' checkboxes
			}				
			
			// Hides the 'How many size dimensions' and width/height questions if the conditions aren't met
			if (!(glass_type_check) && !(film_other_check)){
				show = show.not(gm_ph_ph_num_n_med_parent); // remove 'how many size' element 
				gm_ph_ph_num_n_med.prop( 'checked', false); // clear the 'how many size' radio button
				show = show.not(gm_ph_ph_num_n_med_width1_parent).not(gm_ph_ph_num_n_med_height1_parent);
				show = show.not(gm_ph_ph_num_n_med_width2_parent).not(gm_ph_ph_num_n_med_height2_parent);
			}
			
			show.removeClass('hidden');
			gm_ph_ph_set_description();
		});
		
		// Graphic Materials > Photo > Photo > How many > Negative > Film > [Col Char]
		gm_ph_ph_num_n_f.click(function(){
			var value = this.value;
					
			all.addClass('hidden'); // hide everything
			
			// Are any of the boxes checked?
			if (value){
				show = show.add(gm_ph_ph_num_n_f_cha_parent); // add 'film format' element 
			}
			else{
				show = show.not(gm_ph_ph_num_n_f_cha_parent); // remove 'film format' element 
				gm_ph_ph_num_n_f_cha.prop( 'checked', false); // clear the 'film format' checkboxes
			}
			
			// Is either 'Black & White' or 'Both' checked?
			if (value == "Black & White" || value == "Both"){
				show = show.add(gm_ph_ph_num_n_f_b_parent); // add 'is nitrate' element 
			}
			else{
				show = show.not(gm_ph_ph_num_n_f_b_parent); // remove 'is nitrate' element 
				gm_ph_ph_num_n_f_b.prop( 'checked', false); // clear the 'is nitrate' checkboxes
			}
		
			
			show.removeClass('hidden');
			gm_ph_ph_set_description();
		});
		
		
		// Graphic Materials > Photo > Photo > How many > Negative > Film > B&W/Both > [Nitrate]
		gm_ph_ph_num_n_f_b.click(function(){
			gm_ph_ph_set_description();
		});
		
		// Graphic Materials > Photo > Photo > How many > Negative > Film > Col Char > [Film Format]
		gm_ph_ph_num_n_f_cha.click(function(){
			//mm_check = gm_ph_ph_num_n_f_cha.filter('[value=35mm]').prop("checked"); // is '35mm' checked?
			var other_check = gm_ph_ph_num_n_f_cha.filter('[value=Other]').prop("checked"); // is 'Other' checked?
			var glass_check = gm_ph_ph_num_n_g.filter(":checked").length;
			
			all.addClass('hidden'); // hide everything
				
			// need to add in extra check for negative glass value
			if (other_check || glass_check > 0){
				show = show.add(gm_ph_ph_num_n_med_parent); // add 'how many size' element 
			}
			else{
				show = show.not(gm_ph_ph_num_n_med_parent); // remove 'how many size' element 
				gm_ph_ph_num_n_med.prop( 'checked', false); // clear the 'how many size' radio button
				show = show.not(gm_ph_ph_num_n_med_width1_parent).not(gm_ph_ph_num_n_med_height1_parent);
				show = show.not(gm_ph_ph_num_n_med_width2_parent).not(gm_ph_ph_num_n_med_height2_parent);
			}
			
			show.removeClass('hidden');
			gm_ph_ph_set_description();
		});
		
		// Graphic Materials > Photo > Photo > How many > Negative > Glass
		gm_ph_ph_num_n_g.click(function(){
			var value = this.value;
			var film_other_check = gm_ph_ph_num_n_f_cha.filter('[value=Other]').prop("checked"); // is 'Other' checked for Film Format?
			
			all.addClass('hidden'); // hide everything
			
			if (value || film_other_check){
				show = show.add(gm_ph_ph_num_n_med_parent); // add 'how many size' element 
			}
			else{
				show = show.not(gm_ph_ph_num_n_med_parent); // remove 'how many size' element 
				gm_ph_ph_num_n_med.prop( 'checked', false); // clear the 'how many size' radio button
				show = show.not(gm_ph_ph_num_n_med_width1_parent).not(gm_ph_ph_num_n_med_height1_parent);
				show = show.not(gm_ph_ph_num_n_med_width2_parent).not(gm_ph_ph_num_n_med_height2_parent);
			}
			
			show.removeClass('hidden');
			gm_ph_ph_set_description();
		});
		
		// Graphic Materials > Photo > Photo > How many > Negative > Film/Glass > [How many size]
		gm_ph_ph_num_n_med.click(function(){
			var value = this.value;
			
			all.addClass('hidden'); // hide everything
						
			if (value){
				show = show.add(gm_ph_ph_num_n_med_width1_parent).add(gm_ph_ph_num_n_med_height1_parent);
				if (value == "2"){
					show = show.add(gm_ph_ph_num_n_med_width2_parent).add(gm_ph_ph_num_n_med_height2_parent);
				}
				else{
					show = show.not(gm_ph_ph_num_n_med_width2_parent).not(gm_ph_ph_num_n_med_height2_parent);
				}
			}
			
			show.removeClass('hidden');
			gm_ph_ph_set_description();
		});
		
		// Graphic Materials > Photo > Photo > How many > Negative > Film/Glass > Sizes > [Width1]
		gm_ph_ph_num_n_med_width1.on('input',function(){
			gm_ph_ph_set_description(); // update the RAD Description
		});
		
		// Graphic Materials > Photo > Photo > How many > Negative > Film/Glass > Sizes > [Height1]
		gm_ph_ph_num_n_med_height1.on('input',function(){
			gm_ph_ph_set_description(); // update the RAD Description
		});
		
		// Graphic Materials > Photo > Photo > How many > Negative > Film/Glass > Sizes > [Width2]
		gm_ph_ph_num_n_med_width2.on('input',function(){
			gm_ph_ph_set_description(); // update the RAD Description
		});
		
		// Graphic Materials > Photo > Photo > How many > Negative > Film/Glass > Sizes > [Height2]
		gm_ph_ph_num_n_med_height2.on('input',function(){
			gm_ph_ph_set_description(); // update the RAD Description
		});
		
		// Graphic Materials > Photo > Photo(s) > How many > Slide > [How many]
		gm_ph_ph_num_s.on('input', function(){
			var num_photos = parseInt(gm_ph_ph.val(), 10);
			var num_photo_slides = parseInt(this.value, 10);
			var num_checked = gm_ph_ph_num_s_num.filter(":checked").length; // number of items checked in 'colour characteristics (slide(s))
			
			// clear current number if larger than total number of photos
			if (num_photo_slides > num_photos){
				$(this).val('');
			}
			
			// Clear next section checkboxes if amount is 1 or less
			if (!(num_photo_slides) || (num_photo_slides == 1 && num_checked > 1)){
				gm_ph_ph_num_s_num.prop( 'checked', false ); // clear 'colour characteristics' checkboxes
				gm_ph_ph_num_s_num_cha.prop( 'checked', false); // clear 'slide format' checkboxes
			}
			
			gm_ph_ph_set_description();
		});
		
		// Graphic Materials > Photo > Photo(s) > How many > Slide > [Colour Char]
		gm_ph_ph_num_s_num.click(function(){
			// Prevent invalid number of boxes from being checked
			var num_photos = parseInt(gm_ph_ph.val(), 10);
			var num_photo_slides = parseInt(gm_ph_ph_num_s.val(),10);
			var num_phototype_checked = gm_ph_ph_num_s_num.filter(":checked").length;
			if ( (num_photo_slides <= 1 || num_photos <= 1) && num_phototype_checked >1  ){
				this.checked = false;
			}
			
			all.addClass('hidden'); // hide everything
			
			// Determine which boxes are being checked
			var gm_ph_ph_num_s_num_check_b = gm_ph_ph_num_s_num.filter('[value="b&w"]').prop("checked"); //is 'b&w' checked?
			var gm_ph_ph_num_s_num_check_c = gm_ph_ph_num_s_num.filter('[value="col."]').prop("checked"); //is 'col.' checked?
			
			if (gm_ph_ph_num_s_num_check_b || gm_ph_ph_num_s_num_check_c){
				show = show.add(gm_ph_ph_num_s_num_cha_parent); // add 'slide format' 
			}
			else{
				show = show.not(gm_ph_ph_num_s_num_cha_parent); // remove 'slide format' 
				gm_ph_ph_num_s_num_cha.prop( "checked", false); // clear 'slide format' checkboxes
			}
									
			show.removeClass('hidden');
			gm_ph_ph_set_description();
		});
		
		// Graphic Materials > Photo > Photo(s) > How many > Slide > Colour Char > [Format]
		gm_ph_ph_num_s_num_cha.click(function(){
			// Prevent invalid number of boxes from being checked
			var num_photos = parseInt(gm_ph_ph.val(), 10);
			var num_photo_slides = parseInt(gm_ph_ph_num_s.val(),10);
			var num_checked = gm_ph_ph_num_s_num_cha.filter(":checked").length;
			
			if ( ((num_photo_slides == 1 || num_photos == 1) && num_checked >1 ) || num_checked > 2 ){
				this.checked = false;
			}
			gm_ph_ph_set_description();
		});
		
		// Graphic Materials > Photo > Album > [How many al]
		gm_ph_a.on('input', function(){
			var num_albums = parseInt(this.value, 10);
			
			if (num_albums){
				show = show.add(gm_ph_a_num_parent); // add 'colour characteristics' to list of items to show
			}
			else{
				all.addClass('hidden'); // hide everything
				show = gm_parent.add(gm_ph_parent).add(gm_ph_a_parent); // elements to show
				gm_ph_a_num.val(''); // clear the 'how many photos' box
				gm_ph_a_num_num_aprx.prop('checked', false); // clear the 'is number aprx' text box
				gm_ph_a_num_num.prop('checked', false); // clear the 'colour characteristics' radio button
				gm_ph_a_num_num_cha.prop('checked', false); // clear the 'how many size' radio button
			}
			
			show.removeClass('hidden');
			gm_ph_a_set_description(); // update the RAD Description
		});
		
		// Graphic Materials > Photo > Album > How many al > [How many ph]
		gm_ph_a_num.on('input', function(){
			var num_photos = parseInt(this.value, 10);
			
			if (num_photos){
				show = show.add(gm_ph_a_num_num_parent); // add 'colour characteristics' to list of items to show
				if (num_photos > 1){
					show = show.add(gm_ph_a_num_num_aprx_parent); // add 'is number aprx' to list of items to show
				}
			}
			else{
				all.addClass('hidden'); // hide everything
				show = gm_parent.add(gm_ph_parent).add(gm_ph_a_parent); // elements to show
				gm_ph_a_num_num_aprx.prop('checked', false); // clear the 'is number aprx' text box
				gm_ph_a_num_num.prop('checked', false); // clear the 'colour characteristics' radio button
				gm_ph_a_num_num_cha.prop('checked', false); // clear the 'how many size' radio button
			}
			
			show.removeClass('hidden');
			gm_ph_a_set_description(); // update the RAD Description
		});
		
		// Graphic Materials > Photo > Album > How many al > How many ph > [Approximate]
		gm_ph_a_num_num_aprx.change(function(){
			gm_ph_a_set_description(); // update the RAD Description
		});
		
		// Graphic Materials > Photo > Album > How many al > How many ph > [Col char]
		gm_ph_a_num_num.change(function(){
			var value=this.value;
			if (value){
				show = show.add(gm_ph_a_num_num_cha_parent); // add 'how many photo size' to the list of items to show	
			}
			
			show.removeClass('hidden');
			gm_ph_a_set_description(); // update the RAD Description
		});
		
		// Graphic Materials > Photo > Album > How many al > How many ph > Col char > [Sizes]
		gm_ph_a_num_num_cha.change(function(){
			var value=this.value;
			show = show.add(gm_ph_a_num_num_cha_width1_parent);
			show = show.add(gm_ph_a_num_num_cha_height1_parent);
			
			if (value == "2"){
				show = show.add(gm_ph_a_num_num_cha_width2_parent);
				show = show.add(gm_ph_a_num_num_cha_height2_parent);					
			}
			
			
			show.removeClass('hidden');
			gm_ph_a_set_description(); // update the RAD Description
		});
		
		// Graphic Materials// Graphic Materials > Photo > Album > How many al > How many ph > Col char > Sizes > [Width1]
		gm_ph_a_num_num_cha_width1.on('input',function(){
			gm_ph_a_set_description(); // update the RAD Description
		});
		
		// Graphic Materials// Graphic Materials > Photo > Album > How many al > How many ph > Col char > Sizes > [Height1]
		gm_ph_a_num_num_cha_height1.on('input',function(){
			gm_ph_a_set_description(); // update the RAD Description
		});
		
		// Graphic Materials// Graphic Materials > Photo > Album > How many al > How many ph > Col char > Sizes > [Width2]
		gm_ph_a_num_num_cha_width2.on('input',function(){
			gm_ph_a_set_description(); // update the RAD Description
		});
		
		// Graphic Materials// Graphic Materials > Photo > Album > How many al > How many ph > Col char > Sizes > [Height2]
		gm_ph_a_num_num_cha_height2.on('input',function(){
			gm_ph_a_set_description(); // update the RAD Description
		});
		
		// Reset button
		$( 'button, input[type=reset]').click( function(){
			var form = $('#live_form');
			form.find('input:text').val(''); // clear textboxes
			form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected'); // clear radio buttons and checkboxes
			
			all.addClass('hidden'); //hide all the form questions. They will be revealed as needed
			
			description.text( "RAD Physical Description" ); // set initial RAD Description text
			//description.text(description_text); // Write the description text to the page
		});
		
		// Graphic Materials > Print > [How many]
		gm_pr.on( 'input', function(){
			var value=parseInt(this.value, 10);
			
			all.addClass('hidden'); // hide everything
			show = gm_parent.add(gm_pr_parent);
			
			if ( value ){
				show = show.add(gm_pr_num_parent); // add 'type' element 
				if ( value > 1 ){
					show = show.add(gm_pr_num_aprx_parent); // add 'is num apprx' element 
				}
				else{
					show = show.not(gm_pr_num_aprx_parent); // remove 'is num apprx' element 
					gm_pr_num_aprx.prop( 'checked', false); // cleare 'is num apprx' radio button
				}
			}
			else{
				show = show.not(gm_pr_num_parent); // remove 'type' element 
				gm_pr_num.prop( 'checked', false); // clear 'type' radio button
				show = show.not(gm_pr_num_aprx_parent); // remove 'is num apprx' element 
				gm_pr_num_aprx.prop( 'checked', false); // cleare 'is num apprx' radio button
			}
			
			show.removeClass('hidden');
			gm_pr_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Print > How many > [Apprx]
		gm_pr_num_aprx.click(function(){
			gm_pr_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Print > How many > [Type]
		gm_pr_num.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if ( value ){
				show=show.add(gm_pr_num_type_parent); // add 'Col Cha' element 
			}
			
			show.removeClass('hidden');
			gm_pr_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Print > How many > Type > [Col Cha]
		gm_pr_num_type.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if ( value ){
				show=show.add(gm_pr_num_type_cha_parent); // add 'Process' element 
			}
			
			show.removeClass('hidden');
			gm_pr_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Print > How many > Type > Col Cha > [Process]
		gm_pr_num_type_cha.click(function(){
			var value=this.value;
			var num_proc=gm_pr_num_type_cha.filter(":checked").length; // # checked in 'Process'
			var unknown_check=gm_pr_num_type_cha.filter('[value=Unknown]').prop("checked"); // is 'Unknown' checked?
			
			all.addClass('hidden'); // hide everything
	
			if ( ( num_proc > 3 ) || ( num_proc > 1 && ( value == "Unknown" || unknown_check ) ) ){
				// Make sure no more than 3 checked, and Unknown not checked in combination with anything else
				this.checked = false;
			}
			else if ( num_proc ){
				show=show.add(gm_pr_num_type_cha_proc_parent); // add '# of sizes' element 
			}
			else{
				show=show.not(gm_pr_num_type_cha_proc_parent); // remove '# of sizes' element 
			}
			
			show.removeClass('hidden');
			gm_pr_set_description(); // Update the RAD Description
		});
	
		// Graphic Materials > Print > How many > Type > Col Cha > Process > [# of sizes]
		gm_pr_num_type_cha_proc.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if (value){
				show=show.add(gm_pr_num_type_cha_proc_width1_parent).add(gm_pr_num_type_cha_proc_height1_parent);
				if ( value == "2" ){
					show=show.add(gm_pr_num_type_cha_proc_width2_parent).add(gm_pr_num_type_cha_proc_height2_parent);
				}
				else{
					show=show.not(gm_pr_num_type_cha_proc_width2_parent).not(gm_pr_num_type_cha_proc_height2_parent);
				}
			}
			else{
				show=show.not(gm_pr_num_type_cha_proc_width1_parent).not(gm_pr_num_type_cha_proc_height1_parent);
				show=show.not(gm_pr_num_type_cha_proc_width2_parent).not(gm_pr_num_type_cha_proc_height2_parent);
			}
			
			show.removeClass('hidden');
			gm_pr_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Print > How many > Type > Col Cha > Process > # of sizes > [Width 1]
		gm_pr_num_type_cha_proc_width1.on( 'input', function(){
			var width1=this.value;
			var height1 = gm_pr_num_type_cha_proc_height1.val();
			
			all.addClass('hidden'); // hide everything
			
			if ( width1 && height1 ){
				show=show.add(gm_pr_num_type_cha_proc_size_parent); // add 'mount' element 
			}
			else{
				show=show.not(gm_pr_num_type_cha_proc_size_parent); // remove 'mount' element 
			}
			
			show.removeClass('hidden');
			gm_pr_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Print > How many > Type > Col Cha > Process > # of sizes > [Height 1]
		gm_pr_num_type_cha_proc_height1.on( 'input', function(){
			var height1=this.value;
			var width1 = gm_pr_num_type_cha_proc_width1.val();
			
			all.addClass('hidden'); // hide everything
			
			if ( width1 && height1 ){
				show=show.add(gm_pr_num_type_cha_proc_size_parent); // add 'mount' element 
			}
			else{
				show=show.not(gm_pr_num_type_cha_proc_size_parent); // remove 'mount' element 
			}
			
			show.removeClass('hidden');
			gm_pr_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Print > How many > Type > Col Cha > Process > # of sizes > [Width 2]
		gm_pr_num_type_cha_proc_width2.on( 'input', function(){
			gm_pr_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Print > How many > Type > Col Cha > Process > # of sizes > [Height 2]
		gm_pr_num_type_cha_proc_height2.on( 'input', function(){
			gm_pr_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Print > How many > Type > Col Cha > Process > # of sizes > Size > [Mount]
		gm_pr_num_type_cha_proc_size.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if (value){
				show=show.add(gm_pr_num_type_cha_proc_size_width_parent).add(gm_pr_num_type_cha_proc_size_height_parent);
			}
			else{
				show=show.not(gm_pr_num_type_cha_proc_size_width_parent).not(gm_pr_num_type_cha_proc_size_height_parent);
			}
			
			show.removeClass('hidden');
			gm_pr_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Print > How many > Type > Col Cha > Process > # of sizes > Size > Mount > [Mount Width]
		gm_pr_num_type_cha_proc_size_width.on( 'input', function(){
			gm_pr_set_description(); // Update the RAD Description
		});
		
		// Graphic Materials > Print > How many > Type > Col Cha > Process > # of sizes > Size > Mount > [Mount height]
		gm_pr_num_type_cha_proc_size_height.on( 'input', function(){
			gm_pr_set_description(); // Update the RAD Description
		});
		
		// Cartographic Materials > [Specific Material]
		cm.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			var show = cm_parent;
			
			if ( value == "Map" ){
				mat_parent.removeClass('hidden'); // unhide next form element
			}
			else if ( value == "Remote-sensing image" ){
				cm_rsi_parent.removeClass('hidden'); // unhide next form element
			}
			
			show.removeClass('hidden');
		});
		
		// Architectural and Technical Drawings > [Specific Material]
		atd.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			var show = atd_parent;
			
			if ( value ){
				mat_parent.removeClass('hidden'); // unhide next form element
			}
			
			show.removeClass('hidden');
		});
		
		// Map, Arch/Tech drawing > [Num?]
		mat.on('input', function(){
			var value=this.value;
			var cm_selected = cm.filter(":checked").val(); // Is a value picked for Cartographic Material?
			var atd_selected = atd.filter(":checked").val(); // Is a value picked for Arch/Tech Drawing?
			all.addClass('hidden'); // hide everything
			
			if ( value > 0 ){
				// Start off show variable with correct item
				if ( cm_selected ){ show=cm_parent; }
				else if ( atd_selected ){ show=atd_parent; }
				
				show=show.add(mat_parent).add(mat_num_parent); // add needed elements
				if ( value > 1 ){
					show=show.add(mat_num_aprx_parent); // add 'is apprx?' element
				}
			}
			
			show.removeClass('hidden');
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > [Apprx?]
		mat_num_aprx.click(function(){
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > [Mult?]
		mat_num.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			show=show.add(mat_num_ms_parent); // add 'include overlays?' element
			if ( value == "Yes" ){
				show=show.add(mat_num_ms_num_parent); // add 'how many sheets?' element
			}
			else{
				show=show.not(mat_num_ms_num_parent); // remove 'how many sheets?' element
			}
			
			show.removeClass('hidden');
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Yes > [# of sheets?]
		mat_num_ms_num.on('input', function(){
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > [Overlays?]
		mat_num_ms.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			show=show.add(mat_num_ms_o_parent); // add 'on both sides?' element
			if ( value == "Yes" ){
				show=show.add(mat_num_ms_o_bmnum_parent).add(mat_num_ms_o_onum_parent); // add 'how many?' elements
			}
			else{
				show=show.not(mat_num_ms_o_bmnum_parent).not(mat_num_ms_o_onum_parent); // remove 'how many?' elements
			}
			
			show.removeClass('hidden');
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Yes > [Base Num]
		mat_num_ms_o_bmnum.on('input', function(){
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Yes > [Overlay Num]
		mat_num_ms_o_onum.on('input', function(){
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > [Both?]
		mat_num_ms_o.click(function(){
			all.addClass('hidden'); // hide everything
			show=show.add(mat_num_ms_o_bs_parent); // add 'method of production' element
			show.removeClass('hidden');
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > [Method of Production?]
		mat_num_ms_o_bs.click(function(){
			var value=this.value;
			var num_items=mat.val(); // # of items
			var num_mp=mat_num_ms_o_bs.filter(":checked").length; // # checked in 'MP'
			all.addClass('hidden'); // hide everything
			
			// Are any items checked?
			if ( num_mp > 0 ){
				show=show.add(mat_num_ms_o_bs_mp_parent); // add 'coloured?' element
			}
			// Should I hide upcoming elements if nothing is checked? Seems like a lot of work for not much gain.
			// Make sure no more than 3 are checked
			if ( num_mp > 3 || ( num_mp > num_items ) ){
				this.checked = false;
			}
			// is 'Reproduction' checked?
			var repro_check=mat_num_ms_o_bs.filter('[value=Reproduction]').prop("checked"); 
			if ( repro_check && num_items == "1" ){
				show=show.add(mat_num_ms_o_bs_r_parent); // add 'determine process type?' element
			}
			else{
				show=show.not(mat_num_ms_o_bs_r_parent); // remove 'determine process type?' element
			}
			
			show.removeClass('hidden');
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > Repro > [Determine Type?]
		mat_num_ms_o_bs_r.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if ( value == "Yes" ){
				show=show.add(mat_num_ms_o_bs_r_y_parent); // add 'Line colour?' element
			}
			else{
				show=show.not(mat_num_ms_o_bs_r_y_parent); // remove 'Line colour?' element
			}
			
			show.removeClass('hidden');
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > Repro > Y > [Line Colour?]
		mat_num_ms_o_bs_r_y.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
		
			// hide all subsequent questions
			show=show.not(mat_num_ms_o_bs_r_y_lc_parent).not(mat_num_ms_o_bs_r_y_w_bg_parent).not(mat_num_ms_o_bs_r_y_bg_wdw_parent).not(mat_num_ms_o_bs_r_y_db_cw_parent).not(mat_num_ms_o_bs_r_y_p_cw_parent).not(mat_num_ms_o_bs_r_y_w_bg_parent).not(mat_num_ms_o_bs_r_y_b_w_parent).not(mat_num_ms_o_bs_r_y_db_cw_parent).not(mat_num_ms_o_bs_r_y_lb_parent).not(mat_num_ms_o_bs_r_y_p_cw_parent).not(mat_num_ms_o_bs_r_y_pro_parent).not(mat_num_ms_o_bs_r_y_bg_wdw_parent).not(mat_num_ms_o_bs_r_y_bg_wdw_g_parent).not(mat_num_ms_o_bs_r_y_bg_wdw_p_parent).not(mat_num_ms_o_bs_r_y_gy_parent).not(mat_num_ms_o_bs_r_y_mc_parent);
			// clear values from upcoming elements
			mat_num_ms_o_bs_r_y_lc.prop( 'checked', false ); // ground colour
			mat_num_ms_o_bs_r_y_w_bg.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_b_w.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_db_cw.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_lb.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_p_cw.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_pro.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_bg_wdw.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_bg_wdw_g.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_bg_wdw_p.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_gy.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_mc.prop( 'checked', false );
			
			// Figure out which question to show next
			if ( value.match("White|Blue|Dark Brown|Purple|Black or Gray") ){
				show=show.add(mat_num_ms_o_bs_r_y_lc_parent); // add 'ground colour?' element
			}
			else if ( value == "Light Brown" ){
				mat_num_ms_o_bs_r_y_lb_parent.removeClass('hidden'); // show the appropriate 'select process' element
			}
			else if ( value == "Pink, Red or Orange" ){
				mat_num_ms_o_bs_r_y_pro_parent.removeClass('hidden'); // show the appropriate 'select process' element
			}
			else if ( value == "Green or Yellow" ){
				mat_num_ms_o_bs_r_y_gy_parent.removeClass('hidden'); // show the appropriate 'select process' element
			}
			else if ( value == "Multiple Colours" ){
				mat_num_ms_o_bs_r_y_mc_parent.removeClass('hidden'); // show the appropriate 'select process' element
			}
		
			show.removeClass('hidden');
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > Repro > Y > LC > [Ground Colour?]
		// I structured it this way, instead of building out multiple "What is the colour of the ground?" question blocks
		// because I thought it would be less effort. I may have been incorrect.
		mat_num_ms_o_bs_r_y_lc.click(function(){
			var value=this.value;
			var linecol = mat_num_ms_o_bs_r_y.filter( ":checked" ).val(); // selected line colour value
			all.addClass('hidden'); // hide everything
			
			// clear values from all subsequent questions
			mat_num_ms_o_bs_r_y_w_bg.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_b_w.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_db_cw.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_lb.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_p_cw.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_pro.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_bg_wdw.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_bg_wdw_g.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_bg_wdw_p.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_gy.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_mc.prop( 'checked', false );
			// hide all subsequent questions
			show=show.not(mat_num_ms_o_bs_r_y_w_bg_parent).not(mat_num_ms_o_bs_r_y_bg_wdw_parent).not(mat_num_ms_o_bs_r_y_db_cw_parent).not(mat_num_ms_o_bs_r_y_p_cw_parent).not(mat_num_ms_o_bs_r_y_w_bg_parent).not(mat_num_ms_o_bs_r_y_b_w_parent).not(mat_num_ms_o_bs_r_y_db_cw_parent).not(mat_num_ms_o_bs_r_y_lb_parent).not(mat_num_ms_o_bs_r_y_p_cw_parent).not(mat_num_ms_o_bs_r_y_pro_parent).not(mat_num_ms_o_bs_r_y_bg_wdw_parent).not(mat_num_ms_o_bs_r_y_bg_wdw_g_parent).not(mat_num_ms_o_bs_r_y_bg_wdw_p_parent).not(mat_num_ms_o_bs_r_y_gy_parent).not(mat_num_ms_o_bs_r_y_mc_parent);
			
			// Figure out which question to show next
			if ( value == "Black or Gray" ){
				if ( linecol == "White" ){
					mat_num_ms_o_bs_r_y_w_bg_parent.removeClass('hidden'); // show the 'select process' element
				}
				else{ // not a valid ground colour for selected line colour
					this.checked = false; 
				}
			}
			else if ( value == "Blue" ){
				if ( linecol == "White" ){
					// Nothing special happens here, but the item is selectable
				}
				else{ // not a valid ground colour for selected line colour
					this.checked = false; 
				}
			}
			else if ( value == "Brown" ){
				if ( linecol == "White" ){
					// Nothing special happens here, but the item is selectable
				}
				else{ // not a valid ground colour for selected line colour
					this.checked = false; 
				}
			}
			else if ( value == "Buff to Orange" ){
				if ( linecol == "Black or Gray" ){
					// Nothing special happens here, but the item is selectable
				}
				else{ // not a valid ground colour for selected line colour
					this.checked = false; 
				}
			}
			else if ( value == "Green" ){
				if ( linecol.match("Blue|Purple|Black or Gray") ){
					// Nothing special happens here, but the item is selectable
				}
				else{ // not a valid ground colour for selected line colour
					this.checked = false; 
				}
			}
			else if ( value == "White" ){
				if ( linecol == "Blue" ){
					mat_num_ms_o_bs_r_y_b_w_parent.removeClass('hidden'); // show the 'select process' element
				}
				else{ // not a valid ground colour for selected line colour
					this.checked = false; 
				}
			}
			else if ( value == "White or Dirty White" ){
				if ( linecol == "Black or Gray" ){
					mat_num_ms_o_bs_r_y_bg_wdw_parent.removeClass('hidden'); // show the 'support paper' element
				}
				else{ // not a valid ground colour for selected line colour
					this.checked = false; 
				}
			}
			else if ( value == "Clean White" ){
				if ( linecol == "Dark Brown" ){
					mat_num_ms_o_bs_r_y_db_cw_parent.removeClass('hidden'); // show the 'select process' element
				}
				else if ( linecol == "Purple" ){
					mat_num_ms_o_bs_r_y_p_cw_parent.removeClass('hidden'); // show the 'select process' element
				}
				else{ // not a valid ground colour for selected line colour
					this.checked = false; 
				}
			}
			else if ( value == "Flecked White" ){
				if ( linecol.match("Dark Brown|Purple") ){
					// Nothing special happens here, but the item is selectable
				}
				else{ // not a valid ground colour for selected line colour
					this.checked = false; 
				}
			}
			else if ( value == "Mottled White to Reddish Brown" ){
				if ( linecol == "Dark Brown" ){
					// Nothing special happens here, but the item is selectable
				}
				else{ // not a valid ground colour for selected line colour
					this.checked = false; 
				}
			}

			show.removeClass('hidden');
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > Repro > Y > White > Black or Gray > [Process?]
		mat_num_ms_o_bs_r_y_w_bg.click(function(){
			show=show.add(mat_num_ms_o_bs_r_y_w_bg_parent); // add current element
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > Repro > Y > Blue > White > [Process?]
		mat_num_ms_o_bs_r_y_b_w.click(function(){
			show=show.add(mat_num_ms_o_bs_r_y_b_w_parent); // add current element
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > Repro > Y > Dark Brown > Clean White > [Process?]
		mat_num_ms_o_bs_r_y_db_cw.click(function(){
			show=show.add(mat_num_ms_o_bs_r_y_db_cw_parent); // add current element
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > Repro > Y > Light Brown > [Process?]
		mat_num_ms_o_bs_r_y_lb.click(function(){
			show=show.add(mat_num_ms_o_bs_r_y_lb_parent); // add current element
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > Repro > Y > Purple > Clean White > [Process?]
		mat_num_ms_o_bs_r_y_p_cw.click(function(){
			show=show.add(mat_num_ms_o_bs_r_y_p_cw_parent); // add current element
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > Repro > Y > Pink,Red,Orange > [Process?]
		mat_num_ms_o_bs_r_y_pro.click(function(){
			show=show.add(mat_num_ms_o_bs_r_y_pro_parent); // add current element
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > Repro > Y > Black or Gray > White > [Paper?]
		mat_num_ms_o_bs_r_y_bg_wdw.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			show=show.add(mat_num_ms_o_bs_r_y_bg_wdw_parent); // add current element
			
			// Clear values of subsequent questions
			mat_num_ms_o_bs_r_y_bg_wdw_g.prop( 'checked', false );
			mat_num_ms_o_bs_r_y_bg_wdw_p.prop( 'checked', false );
			// Hide subsequent questions
			show=show.not(mat_num_ms_o_bs_r_y_bg_wdw_g_parent).not(mat_num_ms_o_bs_r_y_bg_wdw_p_parent);
			
			if ( value.match("Glossy") ){
				mat_num_ms_o_bs_r_y_bg_wdw_g_parent.removeClass('hidden');
			}
			else if ( value.match("fibres visible") ){
				mat_num_ms_o_bs_r_y_bg_wdw_p_parent.removeClass('hidden');
			}
			
			show.removeClass('hidden');
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > Repro > Y > Black or Gray > White > Glossy > [Process?]
		mat_num_ms_o_bs_r_y_bg_wdw_g.click(function(){
			show=show.add(mat_num_ms_o_bs_r_y_bg_wdw_g_parent); // add current element
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > Repro > Y > Black or Gray > White > Fibres visible > [Process?]
		mat_num_ms_o_bs_r_y_bg_wdw_p.click(function(){
			show=show.add(mat_num_ms_o_bs_r_y_bg_wdw_p_parent); // add current element
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > Repro > Y > Green,Yellow > [Process?]
		mat_num_ms_o_bs_r_y_gy.click(function(){
			show=show.add(mat_num_ms_o_bs_r_y_gy_parent); // add current element
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > Repro > Y > Multiple > [Process?]
		mat_num_ms_o_bs_r_y_mc.click(function(){
			show=show.add(mat_num_ms_o_bs_r_y_mc_parent); // add current element
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > MP > [Coloured?]
		mat_num_ms_o_bs_mp.click(function(){
			var value=this.value;
			var num = mat.val(); // # of items
			all.addClass('hidden'); // hide everything
			
			// Prevent selection of 'some' items if only 1 item
			if ( num == 1 && value.match("Some|some") ) {
				this.checked = false;
				show=show.not(mat_num_ms_o_bs_mp_c_parent); // remove 'annotated?' element
			}
			else{
				show=show.add(mat_num_ms_o_bs_mp_c_parent); // add 'annotated?' element
			}
			show.removeClass('hidden');
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > MP > Col > [Annotated?]
		mat_num_ms_o_bs_mp_c.click(function(){
			all.addClass('hidden'); // hide everything
			show=show.add(mat_num_ms_o_bs_mp_c_a_parent); // add 'material?' element
			show.removeClass('hidden');
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > MP > Col > Ann > [Material?]
		mat_num_ms_o_bs_mp_c_a.click(function(){
			all.addClass('hidden'); // hide everything
			show=show.add(mat_num_ms_o_bs_mp_c_a_m_parent); // add 'Mounted?' element
			show.removeClass('hidden');
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > MP > Col > Ann > Mat > [Mounted?]
		mat_num_ms_o_bs_mp_c_a_m.click(function(){
			all.addClass('hidden'); // hide everything
			show=show.add(mat_num_ms_o_bs_mp_c_a_m_m_parent); // add '# of dimensions?' element
			show.removeClass('hidden');
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > MP > Col > Ann > Mat > Mnt >[# of sizes?]
		mat_num_ms_o_bs_mp_c_a_m_m.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if ( value ){
				show=show.add(mat_num_ms_o_bs_mp_c_a_m_m_width1_parent).add(mat_num_ms_o_bs_mp_c_a_m_m_height1_parent);
				if ( value == "2" ){
					show=show.add(mat_num_ms_o_bs_mp_c_a_m_m_width2_parent).add(mat_num_ms_o_bs_mp_c_a_m_m_height2_parent);
				}
				else{
					show=show.not(mat_num_ms_o_bs_mp_c_a_m_m_width2_parent).not(mat_num_ms_o_bs_mp_c_a_m_m_height2_parent);
				}
			}
			else{
				show=show.not(mat_num_ms_o_bs_mp_c_a_m_m_width1_parent).not(mat_num_ms_o_bs_mp_c_a_m_m_height1_parent);
				show=show.not(mat_num_ms_o_bs_mp_c_a_m_m_width2_parent).not(mat_num_ms_o_bs_mp_c_a_m_m_height2_parent);
			}
			
			show.removeClass('hidden');
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > MP > Col > Ann > Mnt > Mat > # of sizes > [Width 1]
		mat_num_ms_o_bs_mp_c_a_m_m_width1.on( 'input', function(){
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > MP > Col > Ann > Mnt > Mat > # of sizes > [Height 1]
		mat_num_ms_o_bs_mp_c_a_m_m_height1.on( 'input', function(){
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > MP > Col > Ann > Mnt > Mat > # of sizes > [Width 2]
		mat_num_ms_o_bs_mp_c_a_m_m_width2.on( 'input', function(){
			mat_set_description(); // Update the RAD Description
		});
		
		// Map, Arch/Tech drawing > Num > Mult > Overlays > Both > MP > Col > Ann > Mnt > Mat > # of sizes > [Height 2]
		mat_num_ms_o_bs_mp_c_a_m_m_height2.on( 'input', function(){
			mat_set_description(); // Update the RAD Description
		});
		
		
		// Cartographic Materials > RSI > [Type]
		cm_rsi.click(function(){
			var value=this.value;
			all.addClass('hidden'); // hide everything
			
			if ( value.match("print|Glass") ) {
				show=cm_parent.add(cm_rsi_parent).add(cm_rsi_pg_parent); // add needed elements 
			}
			else {
				show=cm_parent.add(cm_rsi_parent).add(cm_rsi_n_parent); // add needed elements 
			}
			
			show.removeClass('hidden');
			cm_rsi_set_description(); // Update the RAD Description
		});
		
		// Cartographic Materials > RSI > Photo/Glass > [How many?]
		cm_rsi_pg.on('input', function(){
			var value=parseInt(this.value, 10);
			
			all.addClass('hidden'); // hide everything
			show=show.add(cm_rsi_pg_parent);
			
			if ( value ){
				if ( value > 1 ){
					show=show.add(cm_rsi_pg_num_aprx_parent); // add 'is apprx' element 
				}
				else{
					show=show.not(cm_rsi_pg_num_aprx_parent); // remove 'is apprx' element 
				}
				show=show.add(cm_rsi_pg_num_parent); // add '# of dimensions' element 
			}
			else{
				show=show.not(cm_rsi_pg_num_aprx_parent); // remove 'is apprx' alement 
				show=show.not(cm_rsi_pg_num_parent); // remove '# of dimensions' element 
			}
			
			show.removeClass('hidden');
			cm_rsi_set_description(); // Update the RAD Description
		});
		
		// Cartographic Materials > RSI > Photo/Glass > How many > [Apprx]
		cm_rsi_pg_num_aprx.click(function(){
			cm_rsi_set_description(); // Update the RAD Description
		});
		
		// Cartographic Materials > RSI > Photo/Glass > How many > [# of sizes]
		cm_rsi_pg_num.click(function(){
			var value=this.value;
			all.addClass('hidden');
			
			if ( value ){
				show=show.add(cm_rsi_pg_num_amt_width1_parent).add(cm_rsi_pg_num_amt_height1_parent);
				if ( value == "2" ){
					show=show.add(cm_rsi_pg_num_amt_width2_parent).add(cm_rsi_pg_num_amt_height2_parent);
				}
				else{
					show=show.not(cm_rsi_pg_num_amt_width2_parent).not(cm_rsi_pg_num_amt_height2_parent);
				}
			}
			else{
				show=show.not(cm_rsi_pg_num_amt_width1_parent).not(cm_rsi_pg_num_amt_height1_parent);
				show=show.not(cm_rsi_pg_num_amt_width2_parent).not(cm_rsi_pg_num_amt_height2_parent);
			}
			
			show.removeClass('hidden');
			cm_rsi_set_description(); // Update the RAD Description
		});
		
		// Cartographic Materials > RSI > Photo/Glass > How many > # of sizes > [Width 1]
		cm_rsi_pg_num_amt_width1.on( 'input', function(){
			cm_rsi_set_description(); // Update the RAD Description
		});
		
		// Cartographic Materials > RSI > Photo/Glass > How many > # of sizes > [Height 1]
		cm_rsi_pg_num_amt_height1.on( 'input', function(){
			cm_rsi_set_description(); // Update the RAD Description
		});
		
		// Cartographic Materials > RSI > Photo/Glass > How many > # of sizes > [Width 2]
		cm_rsi_pg_num_amt_width2.on( 'input', function(){
			cm_rsi_set_description(); // Update the RAD Description
		});
		
		// Cartographic Materials > RSI > Photo/Glass > How many > # of sizes > [Height 2]
		cm_rsi_pg_num_amt_height2.on( 'input', function(){
			cm_rsi_set_description(); // Update the RAD Description
		});
		
		// Cartographic Materials > RSI > Negative > [How many?]
		cm_rsi_n.on( 'input', function(){
			var value=parseInt(this.value, 10);
			
			all.addClass('hidden'); // hide everything
			show=show.add(cm_rsi_n_parent);
			
			if ( value ){
				if ( value > 1 ){
					show=show.add(cm_rsi_n_num_aprx_parent); // add 'is apprx' element 
				}
				else{
					show=show.not(cm_rsi_n_num_aprx_parent); // remove 'is apprx' element 
				}
				show=show.add(cm_rsi_n_num_parent); // add 'include count' element 
			}
			else{
				show=show.not(cm_rsi_n_num_aprx_parent); // remove 'is apprx' element 
				show=show.not(cm_rsi_n_num_parent); // remove 'include count' element 
			}
			
			show.removeClass('hidden');
			cm_rsi_set_description(); // Update the RAD Description
		});
		
		// Cartographic Materials > RSI > Negative > How many > [Apprx]
		cm_rsi_n_num_aprx.click(function(){
			cm_rsi_set_description(); // Update the RAD Description
		});
		
		// Cartographic Materials > RSI > Negative > How many > [Include count?]
		cm_rsi_n_num.click(function(){
			var value=this.value;
			all.addClass('hidden');
			
			if (value == "Yes"){
				show=show.add(cm_rsi_n_num_cnt_parent); // add 'how many frames' element
			}
			else{
				show=show.not(cm_rsi_n_num_cnt_parent); // remove 'how many frames' element 
			}
			
			show=show.add(cm_rsi_n_num_cnt_dia_parent); // add 'diameter' element 
			
			show.removeClass('hidden');
			cm_rsi_set_description(); // Update the RAD Description
		});
		
		// Cartographic Materials > RSI > Negative > How many > Include count > [Count]
		cm_rsi_n_num_cnt.on( 'input', function(){
			var value=parseInt(this.value, 10);
			
			all.addClass('hidden'); // hide everything
	
			if ( value > 1 ){
				show=show.add(cm_rsi_n_num_cnt_num_aprx_parent); // add 'is apprx' element
			}
			else{
				show=show.not(cm_rsi_n_num_cnt_num_aprx_parent); // remove 'is apprx' element
			}
			
			show.removeClass('hidden');
			cm_rsi_set_description(); // Update the RAD Description
		});
		
		// Cartographic Materials > RSI > Negative > How many > Include count > [Apprx]
		cm_rsi_n_num_cnt_num_aprx.click(function(){
			cm_rsi_set_description(); // Update the RAD Description
		});
		
		// Cartographic Materials > RSI > Negative > How many > Include count > [Diameter]
		cm_rsi_n_num_cnt_dia.on( 'input', function(){
			cm_rsi_set_description(); // Update the RAD Description
		});
		
});
