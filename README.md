# RAD Physical Description Builder

## Overview

**Purpose:** This tool is designed to make it easier to create archival physical descriptions (extents) that conform to the Canadian archival description standard Rules for Archival Description (revised 2008). The tool currently builds archival physical descriptions for:

- textual records
- graphic materials (collages, drawings, paintings, photographs, non-photographic prints)
- cartographic materials (maps, remote-sensing images)
- architectural drawings 
- technical drawings

**Intended Audience:** Archivists, archives student workers, archives volunteers, archival studies students

**Live example:** https://search.nbca.unbc.ca/atom_tools/rad_form.html

---

## Disclaimers

Although RAD is a very prescriptive archival standard, many decisions about how to craft a physical description are still left to the purview of the archivist. That means a physical description for the same archival material can take different forms and still be considered a "correct" RAD description. The output from this tool is one archivist’s interpretation of the RAD standard. This tool should also not be considered a complete guide for all the forms a physical description can take. This tool may not meet your physical description needs in cases of very unusual archival material. Please refer to the RAD standard.

Due to time and technical limitations, this tool currently only builds RAD physical descriptions for textual records, graphic materials, cartographic materials, architectural drawings, and technical drawings. We currently have no plans to expand this tool to include the following: moving images, sound recordings, electronic records, microform, objects, or philatelic records. This is due to the complexity of descriptions for these material types. We also have less of this material in our own archives at UNBC (Northern BC Archives), so we have a minimal use-case. We welcome other institutions/archivists/coders to contribute to the tool by adding further material types.

---

## Usage

To use the tool locally:

1. Download **rad_form.html** and the **rad_form_files** folder
2. Open **rad_form.html** in your modern, up-to-date browser of choice (tested in Chrome, Firefox and Edge. IE is a no-go. Safari et al. are a mystery)

That's it. The tool should be ready to use.

To put the tool up on your website, simply place **rad_form.html** and the **rad_form_files** folder wherever public files are supposed to go on your webserver.
There are no server-side requirements, all the fun happens in the client-side browser.

---

## Sources

* Conditional form example: https://formden.com/blog/conditional-form-field
* Bureau of Canadian Archivists. "Rules for Archival Description - Chapter 1: General Rules for Description (Revised 2008)." Canadian Council on Archives. http://www.cdncouncilarchives.ca/RAD/RAD_Chapter01_July2008.pdf 
* Bureau of Canadian Archivists. "Rules for Archival Description - RAD Chapter 3: Textual Records (Revised 2008)." Canadian Council on Archives. http://www.cdncouncilarchives.ca/RAD/RAD_Chapter03_Aug2001.pdf 
* Bureau of Canadian Archivists. "Rules for Archival Description - RAD Chapter 4: Graphic Materials (Revised 2008)." Canadian Council on Archives. http://www.cdncouncilarchives.ca/RAD/RAD_Chapter04_July2008.pdf 
* Bureau of Canadian Archivists. "Rules for Archival Description - RAD Chapter 5: Cartographic Materials (Revised 2008)." Canadian Council on Archives. http://www.cdncouncilarchives.ca/RAD/RAD_Chapter05_July2008.pdf 
* Bureau of Canadian Archivists. "Rules for Archival Description - RAD Chapter 6: Architectural and Technical Drawings (Revised 2008)." Canadian Council on Archives. http://www.cdncouncilarchives.ca/RAD/RAD_Chapter06_March%202008.pdf 
* Kissel, Eléonore, and Erin Vigneau. Architectural Photoreproductions: A Manual for Identification and Care. New Castle, DE: Oak Knoll Press, 2009.
* Image Permanence Institute. Rochester Institute of Technology. Graphics Atlas. http://graphicsatlas.org

---

## Rights

* Copyright © 2018 University of Northern British Columbia
* Released under an MIT license: https://opensource.org/licenses/MIT
* Designed by Kim Stathers, Archivist/Librarian, Northern BC Archives (UNBC Library)
* Developed by David Pettitt, Systems Administrator I, UNBC Library

### Apologia

I (David) am not a software developer by training or inclination. I write small scripts to make life easier for myself and those around me. This is one of those things that started as "sure, I have some free time, how hard could it be" and ended in more complexity than expected. If you haven't yet looked at the JavaScript, I recommend preparing some [Eye bleach](https://www.reddit.com/r/Eyebleach/) ahead of time.

---

## Feedback/Questions

Contact: <archives@unbc.ca>

Please let us know if you find this tool helpful, if you find any issues with the tool, and/or if you have implemented it at your own institution.